package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.SRI.SRIDocumentoAutorizado;
import atrums.modelo.SRI.SRIDocumentoRecibido;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacion;
import atrums.persistencia.ServiceRecibido;

public class MonitorearRetencionesSRI implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearRetencionesSRI.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearRetencionesSRI(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpen = null;
		
		try {
			ConfService confService = new ConfService();
			
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			conHome = home.getConneccion(dataSourceOpenbravo);
			
			if(conHome != null && this.variablesServicio.isExtDocumentoRetSRI()){
				if(home.getBDDDocumentoBaseProcesar("RT" ,conHome)){
					documentoBase = home.getDocumentoBase();
					
					/**Codigo para evitar demaciado demanda de recurso**/
					
					if(this.variablesServicio.getDocumentRetSRI().equals(documentoBase.getNrodocumento())){
						int auxConteo = this.variablesServicio.getRepeticionRetSRI();
						auxConteo = auxConteo + 1;
						this.variablesServicio.setRepeticionRetSRI(auxConteo);
						
						if(auxConteo == 5){
							this.variablesServicio.setContinuarRetSRI(false);
							this.variablesServicio.setRepeticionRetSRI(0);
						}
					}else{
						this.variablesServicio.setDocumentRetSRI(documentoBase.getNrodocumento());
						this.variablesServicio.setRepeticionRetSRI(0);
					}
					
					/***/
					
					log.info("Procesando documento retencion: " + documentoBase.getNrodocumento());
					home.saveLogServicio(conHome, "Procesando documento retencion: " + documentoBase.getNrodocumento());
					if(documentoBase.getEstado().equals("N") || 
							documentoBase.getEstado().equals("DEV") || 
							documentoBase.getEstado().equals("NOA") || 
							documentoBase.getEstado().equals("NOR")){
						ExecutorService exGenerar = Executors.newCachedThreadPool();
						GenerarDocumentosRetencion documento = new GenerarDocumentosRetencion(documentoBase, dataSourceOpenbravo);
						
						Future<?> future = exGenerar.submit(documento);
						
						try{
					        future.get(5, TimeUnit.MINUTES); 
						}catch(Exception ex){
							future.cancel(true);
							log.error(ex.getMessage(), ex);
						}

						exGenerar.shutdownNow();
						
						if(documento.getFileString() != null){
							
							log.info("Autorizando documento retencion: " + documentoBase.getNrodocumento());
							home.saveLogServicio(conHome, "Autorizando documento retencion: " + documentoBase.getNrodocumento());
							
							SRIDocumentoAutorizado autorizadoPre = new SRIDocumentoAutorizado();
							SRIDocumentoRecibido recibido = new SRIDocumentoRecibido();
							
							ServiceAutorizacion serviceAutorizacionPre = new ServiceAutorizacion(confService, 
									documento.getEmpresaPrincipal().getAmbiente(), 
									documento.getClaveAcceso());
							autorizadoPre = serviceAutorizacionPre.CallAutorizado();
							
							if(!autorizadoPre.getEstadoespecifico().equals("AUT")){
								ServiceRecibido serviceRecibido = new ServiceRecibido(confService, 
										documento.getEmpresaPrincipal().getAmbiente(), documento.getFileString());
								recibido = serviceRecibido.CallRecibido();
							}else{
								recibido.setEstado("RECIBIDO");
								recibido.setEstadoespecifico("REC");
							}
							
							if(recibido.getEstadoespecifico() != null){
								
								if(recibido.getMensaje() != null){
									if(recibido.getMensaje().indexOf("CLAVE ACCESO REGISTRADA") != -1){
										recibido.setEstadoespecifico("REC");
										recibido.setMensaje("");
									}
								}
								
								documentoBase.setEstado(recibido.getEstadoespecifico());
								documentoBase.setClaveacceso(documento.getClaveAcceso());
								documentoBase.setMensaje(recibido.getMensaje());
								if(recibido.getInformacion() != null){
									documentoBase.setMensaje(recibido.getMensaje() + recibido.getInformacion().replace("'", ""));
								}
							}
							
							home.setDocumentoBase(documentoBase);
							
							if(home.actualizarEstado(conHome)){
								conHome.commit();
								if(recibido.getEstadoespecifico().equals("REC")){
									SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
									ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
											documento.getEmpresaPrincipal().getAmbiente(), 
											documentoBase.getClaveacceso());
									autorizado = serviceAutorizacion.CallAutorizado();
									
									if(autorizado.getEstadoespecifico() != null){
										documentoBase.setEstado(autorizado.getEstadoespecifico());
										documentoBase.setFechaautorizacion(autorizado.getFechaAutorizacion());
										documentoBase.setNroautorizacion(autorizado.getNumeroAutorizacion());
										
										if(autorizado.getInformacion() != null){
											documentoBase.setMensaje(autorizado.getMensaje() + autorizado.getInformacion().replace("'", ""));
										}else{
											documentoBase.setMensaje(autorizado.getMensaje());
										}
										
									}
									
									home.setDocumentoBase(documentoBase);
									if(home.actualizarEstado(conHome)){
										log.info("Documento retencion autorizado: " + documentoBase.getNrodocumento());
										home.saveLogServicio(conHome, "Documento retencion autorizado: " + documentoBase.getNrodocumento());
										conHome.commit();
									}else{
										conHome.rollback();
									}
								}else{
									conHome.rollback();
								}
							}else{
								conHome.rollback();
							}
						}else{
							documentoBase.setEstado("DEV");
							documentoBase.setMensaje(documento.getMensaje());
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								home.saveLogServicio(conHome, "Documento retencion no procesado: " + documentoBase.getNrodocumento() + " - " + documento.getMensaje());
								conHome.commit();
							}else{
								conHome.rollback();
							}
						}
					}else if(documentoBase.getEstado().equals("REC")){
						SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
						ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
						BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
						OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
								empresaPrincipal, 
								confService);
						conOpen = openbravo.getConneccion(dataSourceOpenbravo);
						
						if(openbravo.getEmpresaPrincipal(conOpen)){
							ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
									openbravo.getEmpresaPrincipal().getAmbiente(), 
									documentoBase.getClaveacceso());
							autorizado = serviceAutorizacion.CallAutorizado();
							
							if(autorizado.getEstadoespecifico() != null){
								documentoBase.setEstado(autorizado.getEstadoespecifico());
								documentoBase.setFechaautorizacion(autorizado.getFechaAutorizacion());
								documentoBase.setNroautorizacion(autorizado.getNumeroAutorizacion());
								documentoBase.setMensaje(autorizado.getMensaje());
							}
							
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								conHome.commit();
							}else{
								conHome.rollback();
							}
						}
					}else{
						log.info("Esperando documentos del SRI...");
						try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
						this.variablesServicio.setExtDocumentoRetSRI(false);
					}
				}else{
					log.info("Esperando documentos...");
					try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoRetSRI(false);
				}
			}else{
				log.info("Esperando conexi�n...");
				this.variablesServicio.setExtDocumentoRetSRI(false);
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpen != null) conOpen.close(); } catch (Exception ex) {};
			try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
		}
	}

}
