package atrums.dao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServEmail;
import atrums.persistencia.OperacionesAuxiliares;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;

public class GenerarDocumentosGuiaRemision implements Runnable{
	static final Logger log = Logger.getLogger(GenerarDocumentosGuiaRemision.class);
	private ConfService confService;
    private ConfBDDOpenbravo confBDDOpenbravo;
    private BDDDocumentoBase documentoBase;
    private OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
    private BDDEmpresaPrincipal empresaPrincipal;
    private String fileString = null;
    private String claveAcceso = null;
    private String mensaje = null;
    private String auxMensajeError = null;
    private DataSource dataSourceOpenbravo;
	
    public GenerarDocumentosGuiaRemision(
    		BDDDocumentoBase documentoBase, 
    		DataSource dataSourceOpenbravo) {
    	this.confService = new ConfService();
    	this.confBDDOpenbravo = new ConfBDDOpenbravo();
    	this.documentoBase = documentoBase;
    	this.dataSourceOpenbravo = dataSourceOpenbravo;
	}
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conOpen = null;
		
		try {
			log.info("Procesando documento: " + documentoBase.getNrodocumento());
			ServiceFacturaNotaRetenPorDocumento documento = new ServiceFacturaNotaRetenPorDocumento(confService, 
					documentoBase.getNrodocumento(), 
					documentoBase.getNroestablecimiento(), 
					documentoBase.getNroemision(), 
					documentoBase.getTipodocumento());
			documento.setFecha(documentoBase.getFechadocumento());
			
			List<ServDocumentoLinea> lineas = documento.callDocumentos();
			
			this.empresaPrincipal = new BDDEmpresaPrincipal();
			OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(this.confBDDOpenbravo, this.empresaPrincipal, this.confService);
			conOpen = openbravo.getConneccion(this.dataSourceOpenbravo);
			
			if(openbravo.getEmpresaPrincipal(conOpen)){
				openbravo.getEmpresaPrincipal().setAmbiente("1");
				empresaPrincipal = openbravo.getEmpresaPrincipal();
				if(lineas.size() > 0){
					ServCliente destinatario = null;
					
					if(lineas.get(0).getDestinatario() != null){
						ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getDestinatario());
						destinatario = serviceCliente.callCliente();
					}else{
						destinatario = new ServCliente(); 
					}
					
					for(int k=0; k<lineas.size(); k++){
						if(lineas.get(k).getMensajeError() != null){
							auxMensajeError = lineas.get(k).getMensajeError();
						}
					}
					
					if(auxMensajeError == null){
						if(destinatario.getIdentificacion() != null){
							
							if(destinatario.getEmail().getEmail() == null){
								ServEmail auxEmail = new ServEmail();
								auxEmail.setIdentificacion(destinatario.getIdentificacion());
								auxEmail.setEmail("N/A");
								destinatario.setEmail(auxEmail);
							}
							
							if(destinatario.getDireccion().getDireccion() == null){
								ServDireccion auxDireccion = new ServDireccion();
								auxDireccion.setIdentificacion(destinatario.getIdentificacion());
								auxDireccion.setDireccion("N/A");
								auxDireccion.setDireccion2("N/A");
								destinatario.setDireccion(auxDireccion);
							}
							
							if(destinatario.getEmail().getEmail() != null && destinatario.getDireccion().getDireccion() != null){
								try {
									File file = File.createTempFile("documento", ".xml", null);
									file.deleteOnExit();
									
									Document document = DocumentHelper.createDocument();
									OutputFormat outputFormat = OutputFormat.createPrettyPrint();
									Element elmguia = null;
									
									String tipoComprobante = "";
									
									elmguia = document.addElement("guiaRemision");
									elmguia.addAttribute("id", "comprobante");
									elmguia.addAttribute("version", "1.0.0");
									tipoComprobante = "06";
									
									final Element elmingtri = elmguia.addElement("infoTributaria");
									
									elmingtri.addElement("ambiente").addText(empresaPrincipal.getAmbiente());
									elmingtri.addElement("tipoEmision").addText(empresaPrincipal.getTipoEmision());
									elmingtri.addElement("razonSocial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getRazonSocial()));
									elmingtri.addElement("nombreComercial").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getNombreComercial()));
									elmingtri.addElement("ruc").addText(empresaPrincipal.getRuc());
									
									DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
									Date fechaDoc = format.parse(documentoBase.getFechadocumento());
									SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
									SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
									
									String secuencial = documentoBase.getNrodocumento();
									for (int i = 0; i < (9 - documentoBase.getNrodocumento().length()); i++) {
										secuencial = "0" + secuencial;
									}
									
									String codNumerico = empresaPrincipal.getCodNumerico();
									
									if(empresaPrincipal.getCodNumerico() != null){
										for (int i = 0; i < (8 - empresaPrincipal.getCodNumerico().length()); i++) {
											codNumerico = "0" + codNumerico;
										}
									}
									
									String claveAcceso = this.auxiliares.generarclaveacceso(
											sdfFormatoClave.format(fechaDoc), 
											tipoComprobante, 
											empresaPrincipal.getRuc(), 
											empresaPrincipal.getAmbiente(), 
											documentoBase.getNroestablecimiento() + documentoBase.getNroemision(), 
											secuencial, 
											codNumerico, 
											empresaPrincipal.getTipoEmision());
									
									elmingtri.addElement("claveAcceso").addText(claveAcceso);
									elmingtri.addElement("codDoc").addText(tipoComprobante);
									elmingtri.addElement("estab").addText(documentoBase.getNroestablecimiento());
									elmingtri.addElement("ptoEmi").addText(documentoBase.getNroemision());
									elmingtri.addElement("secuencial").addText(secuencial);
									elmingtri.addElement("dirMatriz").addText(empresaPrincipal.getDireccion());
									
									Element elminfguia = null;
									elminfguia = elmguia.addElement("infoGuiaRemision");
									
									elminfguia.addElement("dirEstablecimiento").addText(this.auxiliares.normalizacionPalabras(empresaPrincipal.getDireccion()));
									elminfguia.addElement("dirPartida").addText(this.auxiliares.normalizacionPalabras(lineas.get(0).getDirPartida()));
									
									if(lineas.get(0).getTransportita().equals("9999999999999")){
										elminfguia.addElement("razonSocialTransportista").addText("CONSUMIDOR FINAL");
									}else{
										elminfguia.addElement("razonSocialTransportista").addText(this.auxiliares.normalizacionPalabras(lineas.get(0).getRazonSocialTransp()));
									}
									
									if(lineas.get(0).getTransportita().equals("9999999999999")){
										elminfguia.addElement("tipoIdentificacionTransportista").addText("07");
									}else if(lineas.get(0).getTransportita().length() == 13 && lineas.get(0).getTransportita().substring(10, lineas.get(0).getTransportita().length()).equals("001")){
										elminfguia.addElement("tipoIdentificacionTransportista").addText("04");
									}else if(lineas.get(0).getTransportita().length() == 10 && auxiliares.isNumeric(lineas.get(0).getTransportita())){
										elminfguia.addElement("tipoIdentificacionTransportista").addText("05");
									}else{
										elminfguia.addElement("tipoIdentificacionTransportista").addText("06");
									}
									
									elminfguia.addElement("rucTransportista").addText(lineas.get(0).getTransportita());
									
									if(empresaPrincipal.isObliContabi()){
										elminfguia.addElement("obligadoContabilidad").addText("SI");
									}else{
										elminfguia.addElement("obligadoContabilidad").addText("NO");
									}
									
									if(empresaPrincipal.getNumResolucion() != null){
										String numResolucion = empresaPrincipal.getNumResolucion();
										
										for (int i = 0; i < (3 - empresaPrincipal.getNumResolucion().length()); i++) {
											numResolucion = "0" + numResolucion;
										}
										
										elminfguia.addElement("contribuyenteEspecial").addText(numResolucion);
									}
									
									Date fechaIniTransp = format.parse(lineas.get(0).getFechaIniTransp());
									Date fechaFinTransp = format.parse(lineas.get(0).getFechaFinTransp());
									
									elminfguia.addElement("fechaIniTransporte").addText(sdfFormato.format(fechaIniTransp));
									elminfguia.addElement("fechaFinTransporte").addText(sdfFormato.format(fechaFinTransp));
									elminfguia.addElement("placa").addText(lineas.get(0).getPlaca());
									
									Element elmdestinatarios = null;
									elmdestinatarios = elmguia.addElement("destinatarios");
									
									Element elmdestinatario = null;
									elmdestinatario = elmdestinatarios.addElement("destinatario");
									
									elmdestinatario.addElement("identificacionDestinatario").addText(lineas.get(0).getDestinatario());
									
									if(lineas.get(0).getDestinatario().equals("9999999999999")){
										elmdestinatario.addElement("razonSocialDestinatario").addText("CONSUMIDOR FINAL");
									}else{
										elmdestinatario.addElement("razonSocialDestinatario").addText(this.auxiliares.normalizacionPalabras(lineas.get(0).getRazonSocial()));
									}
									
									elmdestinatario.addElement("dirDestinatario").addText(this.auxiliares.normalizacionPalabras(lineas.get(0).getDirDestinatario()));
									elmdestinatario.addElement("motivoTraslado").addText(this.auxiliares.normalizacionPalabras(lineas.get(0).getMotivoTras()));
									/*elmdestinatario.addElement("docAduaneroUnico");
									elmdestinatario.addElement("codEstabDestino");
									elmdestinatario.addElement("ruta");
									elmdestinatario.addElement("codDocSustento");*/
									
									if(auxMensajeError == null){
										Element elmdetguia = null;
										elmdetguia = elmdestinatario.addElement("detalles");
										
										for(int i=0;i<lineas.size();i++){
											Element elmdeta = null;
											elmdeta = elmdetguia.addElement("detalle");
											

											String codProducto = ""; 
											if(lineas.get(i).getProducto().replace(" ", "").toLowerCase().length() > 25){
												codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto().substring(0, 25));
											}else{
												codProducto = auxiliares.limpiarCodigoProducto(lineas.get(i).getProducto());
											}
											
											elmdeta.addElement("codigoInterno").addText(codProducto);
											
											elmdeta.addElement("codigoAdicional").addText(codProducto);
											
											elmdeta.addElement("descripcion").addText(this.auxiliares.normalizacionPalabras(lineas.get(i).getDescripcion()));
											
											elmdeta.addElement("cantidad").addText(lineas.get(i).getCantidad());
										}
										
										final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(file),"utf-8"), outputFormat);
										writer.write(document);
										writer.flush();
										writer.close();
										
										File fileLlave = new File(confService.getDireccionFirma());
										
										if(fileLlave.exists()){
											file = auxiliares.firmarDocumento(file, confService);
											String fileString = null;
											
											if(file != null){
												byte[] bytes = auxiliares.filetobyte(file);
												fileString = new String(bytes, "UTF-8");
											}
											
											file.delete();
											this.fileString = fileString;
											this.claveAcceso = claveAcceso;
											lineas = null;
										}else{
											this.fileString = null;
											this.mensaje = "ERROR / No hay la llave publica para firmar el documento";
											lineas = null;
										}
									}else{
										this.fileString = null;
										this.mensaje = "ERROR INTERNO WEB SERVICE / " + auxMensajeError;
										lineas = null;
									}
								} catch (ParseException ex) {
									// TODO Auto-generated catch block
									log.error(ex.getMessage());	
								} catch (IOException ex) {
									// TODO Auto-generated catch block
									log.error(ex.getMessage());
								}
							}else{
								this.mensaje = "ERROR INTERNO WEB SERVICE / El destinatario no tiene dirección o correo electrónico";
								lineas = null;
							}
						}else{
							this.mensaje = "ERROR INTERNO WEB SERVICE / No hay un destinatario para esta factura, " + (lineas.get(0).getCliente() == null? "específicamente la guia de despacho no tiene destinatario" : ("destinatario: " + lineas.get(0).getDestinatario()));
							lineas = null;
						}
					}else{
						this.mensaje = "ERROR INTERNO WEB SERVICE / " + auxMensajeError;
						lineas = null;
					}
				}else{
					this.mensaje = "ERROR / No hay lineas para este documento";
					lineas = null;
				}
			}
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} finally {
			try { if (conOpen != null) conOpen.close(); } catch (Exception ex) {};
		}
	}
	
	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public String getFileString() {
		return fileString;
	}

	public void setFileString(String fileString) {
		this.fileString = fileString;
	}

	public BDDEmpresaPrincipal getEmpresaPrincipal() {
		return empresaPrincipal;
	}

	public void setEmpresaPrincipal(BDDEmpresaPrincipal empresaPrincipal) {
		this.empresaPrincipal = empresaPrincipal;
	}
}
