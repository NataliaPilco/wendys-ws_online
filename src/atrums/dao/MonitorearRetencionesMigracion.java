package atrums.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.BDDOpenbravo.BDDOadUserEmail;
import atrums.modelo.BDDOpenbravo.BDDOcoRetencionCompraLinea;
import atrums.modelo.BDDOpenbravo.BDDOcoTipoRetencion;
import atrums.modelo.SRI.SRIDocumentoAutorizado;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDetalleRentecion;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServDocumentoLinea;
import atrums.modelo.Servicio.ServEmail;
import atrums.modelo.Servicio.ServRespuestaControl;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacion;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceFacturaNotaRetenPorDocumento;
import atrums.persistencia.ServiceInsertarFEControlDocumentos;
import atrums.persistencia.ServiceRetencionDetallesPorFecha;

public class MonitorearRetencionesMigracion implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearRetencionesMigracion.class);
	private DecimalFormat format2 = new DecimalFormat("0.00");
	private File docFile = null;
	private String correoCliente = null;
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearRetencionesMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpenbravo = null;
		String adicional = "";
		this.docFile = null;
		this.correoCliente = null;
		
		try{
			ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
			ConfService confService = new ConfService();
			
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			
			log.info("Conectando a la bdd para procesar el documento");
			conHome = home.getConneccion(dataSourceOpenbravo);
			
			if(conHome != null && this.variablesServicio.isExtDocumentoFacMig()){
				if(home.getBDDDocumentoBaseMigrar("RT", conHome)){
					documentoBase = home.getDocumentoBase();
					
					/**Codigo para evitar demaciado demanda de recurso**/
					
					if(this.variablesServicio.getDocumentRetMig().equals(documentoBase.getNrodocumento())){
						int auxConteo = this.variablesServicio.getRepeticionRetMig();
						auxConteo = auxConteo + 1;
						this.variablesServicio.setRepeticionRetMig(auxConteo);
						
						if(auxConteo == 5){
							this.variablesServicio.setContinuarRetMig(false);
							this.variablesServicio.setRepeticionRetMig(0);
						}
					}else{
						this.variablesServicio.setDocumentRetMig(documentoBase.getNrodocumento());
						this.variablesServicio.setRepeticionRetMig(0);
					}
					
					/***/
					
					log.info("Migrando documento retencion: " + documentoBase.getNrodocumento());
					home.saveLogServicio(conHome, "Migrando documento retencion al openbravo: " + documentoBase.getNrodocumento());
					
					BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
					OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
							empresaPrincipal, 
							confService);
					conOpenbravo = openbravo.getConneccion(dataSourceOpenbravo);
					openbravo.setDocumentoBase(documentoBase);
			
					log.info("Verificando si la conexion esta cerrada para el documento: " + documentoBase.getNrodocumento());
					if(openbravo.getEmpresaPrincipal(conOpenbravo)){
						empresaPrincipal = openbravo.getEmpresaPrincipal();
						if(openbravo.existeDocumentoImporFCNC(conOpenbravo)){
							if(openbravo.getBDDOadClient(conOpenbravo)){
								String auxEstablecimiento = documentoBase.getNroestablecimiento();
								String auxEmision = documentoBase.getNroemision();
								
								ServiceFacturaNotaRetenPorDocumento auxDocumento = new ServiceFacturaNotaRetenPorDocumento(confService, 
										documentoBase.getNrodocumento(), 
										documentoBase.getNroestablecimiento(), 
										documentoBase.getNroemision(), 
										documentoBase.getTipodocumento());
								auxDocumento.setFecha(documentoBase.getFechadocumento());
								
								List<ServDocumentoLinea> auxlineas = auxDocumento.callDocumentos();
								
								if(auxlineas.size() > 0){
									String numDoc = auxlineas.get(0).getNroComprobanteVenta();
									String numEst = "";
									String numEmi = "";
									
									if(numDoc.length() <= 17){
							            numEst = numDoc.substring(0, 3);
							            numEmi = numDoc.substring(numDoc.indexOf("-") + 1, numDoc.indexOf("-") + 4);
							        }
									
									documentoBase.setNroestablecimiento(numEst);
									documentoBase.setNroemision(numEmi);
									openbravo.setDocumentoBase(documentoBase);
								}
								
								if(openbravo.getBDDOadOrg(conOpenbravo)){
									documentoBase.setNroestablecimiento(auxEstablecimiento);
									documentoBase.setNroemision(auxEmision);
									openbravo.setDocumentoBase(documentoBase);
									
									if(openbravo.getBDDOcDoctype(conOpenbravo)){
										ServiceFacturaNotaRetenPorDocumento documento = new ServiceFacturaNotaRetenPorDocumento(confService, 
												documentoBase.getNrodocumento(), 
												documentoBase.getNroestablecimiento(), 
												documentoBase.getNroemision(), 
												documentoBase.getTipodocumento());
										documento.setFecha(documentoBase.getFechadocumento());
										
										List<ServDocumentoLinea> lineas = documento.callDocumentos();
										
										if(lineas.size() > 0){
											boolean continuar = true;
											
											if(openbravo.existeDato("SELECT count(*) AS total FROM c_bpartner as cb WHERE cb.taxid = '" + lineas.get(0).getRucProveedor() + "';", conOpenbravo)){
												ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getRucProveedor());
												ServCliente cliente = serviceCliente.callCliente();
												if(cliente.getIdentificacion() == null){
													cliente.setIdentificacion(lineas.get(0).getRucProveedor());
													cliente.setNombreComercial(lineas.get(0).getProveedor());
													cliente.setRazonSocial(lineas.get(0).getProveedor());
													
													if(lineas.get(0).getRucProveedor().equals("9999999999999")){
														cliente.setTipo("07");
													}else if(lineas.get(0).getRucProveedor().length() == 10){
														cliente.setTipo("04");
													}else if(lineas.get(0).getRucProveedor().length() == 13 && lineas.get(0).getRucProveedor().substring(10, lineas.get(0).getRucProveedor().length()).equals("001")){
														cliente.setTipo("05");
													}else{
														cliente.setTipo("06");
													}
													
													cliente.setDireccion(new ServDireccion());
													cliente.setEmail(new ServEmail());
												}
												
												if(cliente.getEmail().getEmail() == null){
													ServEmail auxEmail = new ServEmail();
													auxEmail.setIdentificacion(cliente.getIdentificacion());
													auxEmail.setEmail("N/A");
													cliente.setEmail(auxEmail);
													adicional = ", no hay correo electronico";
												}else{
													this.correoCliente = cliente.getEmail().getEmail();
													adicional = ", correo electronico: " + this.correoCliente + " ";
												}
												
												if(cliente.getDireccion().getDireccion() == null){
													ServDireccion auxDireccion = new ServDireccion();
													auxDireccion.setIdentificacion(cliente.getIdentificacion());
													auxDireccion.setDireccion("N/A");
													auxDireccion.setDireccion2("N/A");
													cliente.setDireccion(auxDireccion);
												}
												
												openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
												openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
												openbravo.getOcbPartner().setTipo(cliente.getTipo());
												openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
												openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
												
												if(openbravo.existeDato("SELECT count(*) AS total FROM c_bp_group as cb WHERE upper(cb.name) LIKE '%CLIENTES COMERCIALES%';", conOpenbravo)){
													if(!openbravo.saveBDDOcbGroup(conOpenbravo)){
														conOpenbravo.rollback();
														continuar = false;
													}
												}
												
												if(openbravo.saveBDDOcbPartner(conOpenbravo) && continuar){
													openbravo.getOadUserEmail().setEmail(cliente.getEmail().getEmail());
													if(openbravo.saveBDDOadUser(conOpenbravo)){
														openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
														if(openbravo.saveBDDOcLocation(conOpenbravo)){
															if(openbravo.saveBDDOcbPartnerLocation(conOpenbravo)){
																openbravo.setOadUserEmail(new BDDOadUserEmail());
																if(openbravo.saveBDDOadUserPortal(conOpenbravo)){
																	if(openbravo.existeDato("SELECT count(*) AS total FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTR�NICOS%'", conOpenbravo)){
																		if(openbravo.saveBDDOadRole(conOpenbravo)){
																			if(openbravo.saveBDDOadRoleOrgaccess(conOpenbravo)){
																				if(!openbravo.saveBDDOadWindowAccess(conOpenbravo)){
																					conOpenbravo.rollback();
																					continuar = false;
																				}
																			}else{
																				conOpenbravo.rollback();
																				continuar = false;
																			}
																		}else{
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	if(!openbravo.saveBDDOadUserRoles(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																	
																}else{
																	conOpenbravo.rollback();
																	continuar = false;
																}
															}else{
																conOpenbravo.rollback();
																continuar = false;
															}
														}else{
															conOpenbravo.rollback();
															continuar = false;
														}
													}else{
														conOpenbravo.rollback();
														continuar = false;
													}
												}else{
													conOpenbravo.rollback();
													continuar = false;
												}
											}else{
												ServiceCliente serviceCliente = new ServiceCliente(confService, lineas.get(0).getRucProveedor());
												ServCliente cliente = serviceCliente.callCliente();
												if(cliente.getIdentificacion() != null){
													
													if(cliente.getEmail().getEmail() == null){
														ServEmail auxEmail = new ServEmail();
														auxEmail.setIdentificacion(cliente.getIdentificacion());
														auxEmail.setEmail("N/A");
														cliente.setEmail(auxEmail);
														adicional = ", no hay correo electronico";
													}else{
														this.correoCliente = cliente.getEmail().getEmail();
														adicional = ", correo electronico: " + this.correoCliente + " ";
													}
													
													if(cliente.getDireccion().getDireccion() == null){
														ServDireccion auxDireccion = new ServDireccion();
														auxDireccion.setIdentificacion(cliente.getIdentificacion());
														auxDireccion.setDireccion("N/A");
														auxDireccion.setDireccion2("N/A");
														cliente.setDireccion(auxDireccion);
													}
													
													openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
													openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
													openbravo.getOcbPartner().setTipo(cliente.getTipo());
													openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
													openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
												}else{
													if(cliente.getDireccion().getDireccion() == null){
														cliente.getDireccion().setDireccion(lineas.get(0).getDirProveedor());
													}
												}
												
												openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
												openbravo.setOadUserEmail(new BDDOadUserEmail());
												
												openbravo.getOcbPartner().setIdentificacion(lineas.get(0).getRucProveedor());
												if(openbravo.getBDDOcbPartner(conOpenbravo)){
													if(!openbravo.getBDDOadUser(conOpenbravo)){
														conOpenbravo.rollback();
														continuar = false;
													}else{
														if(cliente.getIdentificacion() != null){
															
															if(cliente.getEmail().getEmail() == null){
																ServEmail auxEmail = new ServEmail();
																auxEmail.setIdentificacion(cliente.getIdentificacion());
																auxEmail.setEmail("N/A");
																cliente.setEmail(auxEmail);
																adicional = ", no hay correo electronico";
															}else{
																this.correoCliente = cliente.getEmail().getEmail();
																adicional = ", correo electronico: " + this.correoCliente + " ";
															}
														}
													}
												}else{
													conOpenbravo.rollback();
													continuar = false;
												}
											}
											
											if(openbravo.getOcbPartner().getId() != null && continuar){
												ServiceRetencionDetallesPorFecha retencionDetallesPorFecha = new ServiceRetencionDetallesPorFecha(confService, 
														lineas.get(0).getSucursal() , 
														documentoBase.getNrodocumento(), 
														documentoBase.getFechadocumento());
												
												List<ServDetalleRentecion> lineasRetencion = retencionDetallesPorFecha.callDocumentos();
												
												if(lineasRetencion.size() > 0){
													String numDoc = documentoBase.getNrodocumento(); //lineas.get(0).getNroComprobanteVenta();
													String numEst = documentoBase.getNroestablecimiento(); //"";
													String numEmi = documentoBase.getNroemision(); //"";
													
													/*if(numDoc.length() <= 17){
											            numEst = numDoc.substring(0, 3);
											            numEmi = numDoc.substring(numDoc.indexOf("-") + 1, numDoc.indexOf("-") + 4);
											            numDoc = numDoc.substring(numDoc.indexOf("-", numDoc.indexOf("-") + 1) + 1, numDoc.length());
											            String auxSecuencial = numDoc;
											            
											            for (int i = 0; i < (9 - numDoc.length()); i++) {
											            	auxSecuencial = "0" + auxSecuencial;
											            }
											            
											            numDoc = auxSecuencial;
											        }*/
													
													openbravo.getOcoRetencionCompra().setNroDocumento(numDoc);
													openbravo.getOcoRetencionCompra().setNroEstablecimiento(numEst);
													openbravo.getOcoRetencionCompra().setNroEmision(numEmi);
													openbravo.getOcoRetencionCompra().setFechaContable(lineas.get(0).getFechaContable());
													openbravo.getOcoRetencionCompra().setNumeroAutorizacion(documentoBase.getNroautorizacion());
													openbravo.getOcoRetencionCompra().setFechaAutorizacion(documentoBase.getFechaautorizacion());
													openbravo.getOcoRetencionCompra().setEstado("AUTORIZADO");
													openbravo.getOcoRetencionCompra().setClaveAcceso(documentoBase.getClaveacceso());
													openbravo.getOcoRetencionCompra().setTipoDocumento("01");
													openbravo.getOcoRetencionCompra().setDescripcion("");
													openbravo.getOcoRetencionCompra().setNrofactura(lineas.get(0).getNroFactura());
													openbravo.getOcoRetencionCompra().setFechaEmision(lineas.get(0).getFechaFactura());
													openbravo.getOcoRetencionCompra().setIdentificacion(lineas.get(0).getRucProveedor());
													
													try{
														SRIDocumentoAutorizado autorizado = new SRIDocumentoAutorizado();
														ServiceAutorizacion serviceAutorizacion = new ServiceAutorizacion(confService, 
																empresaPrincipal.getAmbiente(), 
																documentoBase.getClaveacceso());
														autorizado = serviceAutorizacion.CallAutorizado();
														
														if(autorizado.getDocXML() != null){
															openbravo.getOcoRetencionCompra().setRetencion(autorizado.getDocXML());
															this.docFile = autorizado.getDocFile();
														}
													} catch (Exception ex){}
													
													if(continuar){
														if(openbravo.saveBDDOcoRetencionCompra(conOpenbravo)){
															int linea = 10;
															for(ServDetalleRentecion detalleRentecion: lineasRetencion){
																if(continuar){
																	BDDOcoTipoRetencion ocoTipoRetencion = new BDDOcoTipoRetencion();
																	
																	ocoTipoRetencion.setTipo((detalleRentecion.getTipo().equals("V") ? "IVA" : "") + 
																			(detalleRentecion.getTipo().equals("N") ? "FUENTE" : ""));
																	
																	String codigoRetencion;
																	
																	if(detalleRentecion.getCodigoRetencion().indexOf(".") != -1 || 
																			detalleRentecion.getCodigoRetencion().indexOf(",") != -1){
																		codigoRetencion = String.valueOf(Double.valueOf(detalleRentecion.getCodigoRetencion()).intValue());
																	}else{
																		codigoRetencion = detalleRentecion.getCodigoRetencion();
																	}
																	
																	if(detalleRentecion.getTipo().equals("V")){
																		/*if(codigoRetencion.equals("10")){
																			ocoTipoRetencion.setCodigo("9");
																		} else if(codigoRetencion.equals("20")){
																			ocoTipoRetencion.setCodigo("10");
																		} else if(codigoRetencion.equals("30")){
																			ocoTipoRetencion.setCodigo("1");
																		} else if(codigoRetencion.equals("70")){
																			ocoTipoRetencion.setCodigo("2");
																		} else if(codigoRetencion.equals("100")){
																			ocoTipoRetencion.setCodigo("3");
																		}*/
																		ocoTipoRetencion.setCodigo(codigoRetencion);
																	}else{
																		ocoTipoRetencion.setCodigo(codigoRetencion);
																	}
																	
																	double porcentaje = (Double.valueOf(detalleRentecion.getValorRetencion()) * 100) / (Double.valueOf(detalleRentecion.getBaseImponibleReten()));
																	ocoTipoRetencion.setPorcentaje(String.valueOf(Double.valueOf(Math.round(porcentaje)).intValue()));
																	
																	openbravo.setOcoTipoRetencion(ocoTipoRetencion);
																	
																	if(!openbravo.getBDDOcoTipoRetencion(conOpenbravo)){
																		ocoTipoRetencion.setId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
																		openbravo.setOcoTipoRetencion(ocoTipoRetencion);
																		if(!openbravo.saveBDDOcoTipoRetencion(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	if(!openbravo.getBDDOcoBpRetencionCompra(conOpenbravo)){
																		openbravo.getOcoBpRetencionCompra().setId(UUID.randomUUID().toString().replace("-", "").toUpperCase());
																		openbravo.getOcoBpRetencionCompra().setDescription(ocoTipoRetencion.getCodigo());
																		if(!openbravo.saveBDDOcoBpRetencionCompra(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	if(continuar){
																		BDDOcoRetencionCompraLinea ocoRetencionCompraLinea = new BDDOcoRetencionCompraLinea();
																		ocoRetencionCompraLinea.setLinea(String.valueOf(linea));
																		ocoRetencionCompraLinea.setTipo(ocoTipoRetencion.getTipo());
																		ocoRetencionCompraLinea.setBaseimponible(format2.format(Double.valueOf(detalleRentecion.getBaseImponibleReten())).replace(",", "."));
																		ocoRetencionCompraLinea.setValor(format2.format(Double.valueOf(detalleRentecion.getValorRetencion())).replace(",", "."));
																		
																		openbravo.setOcoRetencionCompraLinea(ocoRetencionCompraLinea);
																		
																		if(!openbravo.saveBDDOcoRetencionCompraLinea(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}else{
																			linea = linea + 10;
																		}
																	}
																}
															}
															
															//Cerrando retencion
															openbravo.terminarRetencion(conOpenbravo);
															
															if(continuar){
																log.info("Enviando a Wendys informacion del documento: " + documentoBase.getNrodocumento());
																ServiceInsertarFEControlDocumentos controlDocumentos = new ServiceInsertarFEControlDocumentos(
																		confService, 
																		documentoBase.getNrodocumento(), 
																		documentoBase.getNroestablecimiento(), 
																		documentoBase.getNroemision(), 
																		documentoBase.getNroautorizacion(), 
																		documentoBase.getFechaautorizacion(), 
																		documentoBase.getFechadocumento(), 
																		"2", 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		documentoBase.getTipodocumento());
																
																ServRespuestaControl respuestaControl = controlDocumentos.callRespuesta();
																
																if(respuestaControl.isRespuesta()){
																	log.info("Datos migrados");
																	conOpenbravo.commit();
																	
																	if(openbravo.getOcbPartner().getIdentificacion().indexOf("9999999999") == -1){
																		try{
																			if(confService.isEnviarCorreo() && this.correoCliente != null){
																				EnviarEmail email = new EnviarEmail(conOpenbravo, openbravo, confService, this.docFile);
																				
																				documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																				documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																				
																				if(email.enviar(this.correoCliente, 
																						documentoBase.getClaveacceso(), 
																						openbravo.getOcoRetencionCompra().getId(), 
																						documentoBase.getTipodocumento(), 
																						documentoBase)){
																					adicional = adicional + ", Correo enviado al: " + this.correoCliente;
																				}else{
																					adicional = adicional + ", Correo no enviado al: " + this.correoCliente;
																				}
																			}else{
																				if(!confService.isEnviarCorreo()){
																					adicional = adicional + ", no esta activo la opci�n de enviar email";
																				}else if(this.correoCliente == null){
																					adicional = adicional + ", no hay correo para enviar email";
																				}
																			}
																		}catch (Exception ex){}
																	}
																	
																	documentoBase.setEstado("MIG");
																	documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes" + adicional);
																	documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																	documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																	if(home.actualizarEstado(conHome)){
																		home.saveLogServicio(conHome, "Documento retencion migrado al openbravo: " + documentoBase.getNrodocumento() + " - " + adicional);
																		conHome.commit();
																	}else{
																		conHome.rollback();
																	}
																}else{
																	conOpenbravo.rollback();
																	log.info("Datos no migrados ver log");
																	home.saveLogServicio(conHome, "Documento retencion no migrado al openbravo: " + documentoBase.getNrodocumento());
																}
															}
														}else{
															if(home.actualizarEstado(conHome)){
																conHome.commit();
															}else{
																conHome.rollback();
															}
															conOpenbravo.rollback();
															continuar = false;
														}
													}
												}else{
													documentoBase.setMensaje("ERROR / No hay lineas para el documento: " + documentoBase.getNrodocumento());
													home.setDocumentoBase(documentoBase);
													if(home.actualizarEstado(conHome)){
														conHome.commit();
													}else{
														conHome.rollback();
													}
												}
											}
										}else{
											documentoBase.setMensaje("ERROR / No hay primera linea para el documento: " + documentoBase.getNrodocumento());
											home.setDocumentoBase(documentoBase);
											if(home.actualizarEstado(conHome)){
												conHome.commit();
											}else{
												conHome.rollback();
											}
										}
									}else{
										log.info("Esperando documento...");
										documentoBase.setMensaje("ERROR / No hay una documento para este tipo de documento");
										home.setDocumentoBase(documentoBase);
										if(home.actualizarEstado(conHome)){
											conHome.commit();
										}else{
											conHome.rollback();
										}
									}
								}else{
									log.info("Esperando sucursal...");
									documentoBase.setMensaje("ERROR / No hay una sucursal para este documento");
									home.setDocumentoBase(documentoBase);
									if(home.actualizarEstado(conHome)){
										conHome.commit();
									}else{
										conHome.rollback();
									}
								}
							}else{
								log.info("Esperando empresa...");
								documentoBase.setMensaje("ERROR / No hay una sucursal para este documento");
								try { if (conOpenbravo != null) conOpenbravo.close(); } catch (Exception ex) {};
								try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
								this.variablesServicio.setExtDocumentoRetMig(false);
							}
						}else{
							documentoBase.setEstado("MIG");
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								conHome.commit();
							}else{
								conHome.rollback();
							}
						}
					}else{
						log.info("Esperando empresa...");
						try { if (conOpenbravo != null) conOpenbravo.close(); } catch (Exception ex) {};
						try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
						this.variablesServicio.setExtDocumentoRetMig(false);
					}
				}else{
					log.info("Esperando documento...");
					try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoRetMig(false);
				}
			}else{
				log.info("Esperando conexi�n...");
				this.variablesServicio.setExtDocumentoRetMig(false);				
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpenbravo != null) conOpenbravo.close(); } catch (Exception ex) {};
			try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
		}
	}
}
