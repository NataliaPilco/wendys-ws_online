package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearClavesNotaCreditoMigracion;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearClavesNotaCreditoMigracion implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearClavesNotaCreditoMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearClavesNotaCreditoMigracion(
			DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			log.info("Migrando documentos nota de credito mediante clave de acceso");
			while(this.variablesServicio.isEstadoFacturasClave()){
				if(this.variablesServicio.isSemaforoDos_M()){
        			this.variablesServicio.setSemaforoDos_M(false);
            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
            		
            		ExecutorService exServiceMigrN = Executors.newFixedThreadPool(1);
			        Runnable runnableMigracionN = new MonitorearClavesNotaCreditoMigracion(
			        		this.dataSourceOpenbravo, 
			        		this.variablesServicio);
			        Future<?> future = exServiceMigrN.submit(runnableMigracionN);
            		
					try{
				        future.get(10, TimeUnit.MINUTES); 
					}catch(Exception ex){
						future.cancel(true);
						log.error(ex.getMessage(), ex);
					}
					
					exServiceMigrN.shutdownNow();
			        
			        this.variablesServicio.setSemaforoDos_M(true);
    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
    				
    				runnableMigracionN = null;
    				exServiceMigrN = null;
    				
    				if(!this.variablesServicio.isExtDocumentoFacClaveN()){
    					this.variablesServicio.setExtDocumentoFacClaveN(true);
			        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
			        }
        		}else{
        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_M());
        			try {log.info("Esperando semaforo...");Thread.sleep(30140);} catch (InterruptedException e) {}
        		}
			}
		}catch(Exception ex){
			this.variablesServicio.setEstadoFacturasClave(false);
			log.error(ex.getMessage(), ex);
		}
	}

}
