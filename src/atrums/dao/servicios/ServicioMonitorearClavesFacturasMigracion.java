package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearClavesFacturaMigracion;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearClavesFacturasMigracion implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearClavesFacturasMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearClavesFacturasMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			log.info("Migrando documentos factura mediante clave de acceso");
			while(this.variablesServicio.isEstadoFacturasClave()){
				if(this.variablesServicio.isSemaforoDos_A()){
        			this.variablesServicio.setSemaforoDos_A(false);
            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
            		
            		ExecutorService exServiceMigrF = Executors.newFixedThreadPool(1);
			        Runnable runnableMigracionF = new MonitorearClavesFacturaMigracion(
			        		this.dataSourceOpenbravo, 
			        		this.variablesServicio);
			        Future<?> future = exServiceMigrF.submit(runnableMigracionF);
            		
					try{
				        future.get(10, TimeUnit.MINUTES); 
					}catch(Exception ex){
						future.cancel(true);
						log.error(ex.getMessage(), ex);
					}
					
					exServiceMigrF.shutdownNow();
			        
			        this.variablesServicio.setSemaforoDos_A(true);
    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
    				
    				runnableMigracionF = null;
    				exServiceMigrF = null;
    				
    				if(!this.variablesServicio.isExtDocumentoFacClaveF()){
    					this.variablesServicio.setExtDocumentoFacClaveF(true);
			        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}			        	
			        }
        		}else{
        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_A());
        			try {log.info("Esperando semaforo...");Thread.sleep(30130);} catch (InterruptedException e) {}
        		}
			}
		}catch(Exception ex){
			this.variablesServicio.setEstadoFacturasClave(false);
			log.error(ex.getMessage(), ex);
		}
	}
}
