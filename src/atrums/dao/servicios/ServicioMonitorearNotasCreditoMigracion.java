package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearNotasCreditoMigracion;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearNotasCreditoMigracion implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearNotasCreditoMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearNotasCreditoMigracion(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Migrando documentos notas openbravo");
				while(this.variablesServicio.isEstadoNotas()){
					if(this.variablesServicio.isSemaforoDos_M()){
	        			this.variablesServicio.setSemaforoDos_M(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
	            		
	            		if(this.variablesServicio.isContinuarNotMig()){
	            			ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
					        Runnable runnableMigracion = new MonitorearNotasCreditoMigracion(
					        		this.dataSourceOpenbravo, 
					        		this.variablesServicio);
					        Future<?> future = exServiceMigr.submit(runnableMigracion);
	            			
							try{
						        future.get(10, TimeUnit.MINUTES); 
							}catch(Exception ex){
								future.cancel(true);
								log.error(ex.getMessage(), ex);
							}
							
							exServiceMigr.shutdownNow();
					        
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
							
		    				runnableMigracion = null;
		    				exServiceMigr = null;
	            		}else{
							this.variablesServicio.setSemaforoDos_M(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_M());
		    				
							this.variablesServicio.setContinuarNotMig(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
	            		
	            		if(!this.variablesServicio.isExtDocumentoNotMig()){
	            			this.variablesServicio.setExtDocumentoNotMig(true);
	            			try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_M());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30050);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoNotas(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
