package atrums.dao.servicios;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearFacturasSRI;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearFacturasSRI implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearFacturasSRI.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearFacturasSRI(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Generando documentos XML factura");
				while(this.variablesServicio.isEstadoFacturas()){
					if(this.variablesServicio.isSemaforoDos_A()){
	        			this.variablesServicio.setSemaforoDos_A(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
	            		
	            		if(this.variablesServicio.isContinuarFacSRI()){
							ExecutorService exService = Executors.newFixedThreadPool(1);
					        Runnable runnableService = new MonitorearFacturasSRI(
					        		this.dataSourceOpenbravo, 
					        		this.variablesServicio);
					        Future<?> future = exService.submit(runnableService);
							
							try{
						        future.get(5, TimeUnit.MINUTES); 
							}catch (TimeoutException ex) {
								boolean c = future.cancel(true);
								log.info("Timeout " + c);
								log.error(ex.getMessage(), ex);
					        }catch(InterruptedException ex){
								log.error(ex.getMessage(), ex);
							}catch(ExecutionException ex){
								log.error(ex.getMessage(), ex);
							}
							
							exService.shutdownNow();
							
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
							
							runnableService = null;
							exService = null;
						}else{
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
		    				
							this.variablesServicio.setContinuarFacSRI(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
				        
	            		if(!this.variablesServicio.isExtDocumentoFacSRI()){
	            			this.variablesServicio.setExtDocumentoFacSRI(true);
				        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_A());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30010);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoFacturas(false);
				log.warn(ex.getMessage());
			}
	}
}
