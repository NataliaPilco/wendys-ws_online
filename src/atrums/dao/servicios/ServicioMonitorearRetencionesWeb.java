package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearRetencionesFechas;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearRetencionesWeb implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearRetencionesWeb.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearRetencionesWeb(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Monitoreando documentos retencion web service");
				while(this.variablesServicio.isEstadoRetenciones()){
					if(this.variablesServicio.isSemaforoUno()){
	        			this.variablesServicio.setSemaforoUno(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
	            		
	            		ExecutorService executor = Executors.newFixedThreadPool(1);
	                	Runnable runnable = new MonitorearRetencionesFechas(this.dataSourceOpenbravo);
	                	Future<?> future = executor.submit(runnable);
	    		    	
						try{
					        future.get(20, TimeUnit.MINUTES); 
						}catch(Exception ex){
							future.cancel(true);
							log.error(ex.getMessage(), ex);
						}
						
						executor.shutdownNow();
				        
						this.variablesServicio.setSemaforoUno(true);
	    				log.info("Semaforo: " + this.variablesServicio.isSemaforoUno());
						
	    				runnable = null;
	    				executor = null;
	    				
	    				try {log.info("Esperando semaforo...");Thread.sleep(600000);} catch (InterruptedException e) {}
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoUno());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30060);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoFacturas(false);
				log.error(ex.getMessage(), ex);
			}
	}

}
