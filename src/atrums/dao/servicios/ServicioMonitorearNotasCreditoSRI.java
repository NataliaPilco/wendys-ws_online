package atrums.dao.servicios;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.MonitorearNotasCreditoSRI;
import atrums.modelo.VariablesServicio;

public class ServicioMonitorearNotasCreditoSRI implements Runnable{
	static final Logger log = Logger.getLogger(ServicioMonitorearNotasCreditoSRI.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public ServicioMonitorearNotasCreditoSRI(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
			try{
				log.info("Generando documentos XML notas");
				while(this.variablesServicio.isEstadoNotas()){
					if(this.variablesServicio.isSemaforoDos_A()){
	        			this.variablesServicio.setSemaforoDos_A(false);
	            		log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
	            		
	            		if(this.variablesServicio.isContinuarNotSRI()){
	            			ExecutorService exService = Executors.newFixedThreadPool(1);
	    			        Runnable runnableService = new MonitorearNotasCreditoSRI(
	    			        		this.dataSourceOpenbravo, 
	    			        		this.variablesServicio);
	    			        Future<?> future = exService.submit(runnableService);
							
							try{
						        future.get(10, TimeUnit.MINUTES); 
							}catch(Exception ex){
								future.cancel(true);
								log.error(ex.getMessage(), ex);
							}
							
							exService.shutdownNow();
							
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
							
							runnableService = null;
							exService = null;
						}else{
							this.variablesServicio.setSemaforoDos_A(true);
		    				log.info("Semaforo: " + this.variablesServicio.isSemaforoDos_A());
		    				
							this.variablesServicio.setContinuarNotSRI(true);
							try{Thread.sleep(600000);}catch (Exception ex) {}
						}
				        
	            		if(!this.variablesServicio.isExtDocumentoNotSRI()){
	            			this.variablesServicio.setExtDocumentoNotSRI(true);
				        	try {log.info("Esperando documentos...");Thread.sleep(500000);} catch (InterruptedException e) {}
				        }
	        		}else{
	        			log.info("Semaforo: trabajando " + this.variablesServicio.isSemaforoDos_A());
	        			try {log.info("Esperando semaforo...");Thread.sleep(30040);} catch (InterruptedException e) {}
	        		}
				}
			}catch(Exception ex){
				this.variablesServicio.setEstadoNotas(false);
				log.error(ex.getMessage(), ex);
			}
	}
}
