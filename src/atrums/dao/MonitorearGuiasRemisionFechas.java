package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceGuiaRemisionFecha;
import atrums.persistencia.ServiceGuiaRemisionFechaPagina;
import atrums.persistencia.ServiceNroPaginasGuiasRemision;

public class MonitorearGuiasRemisionFechas implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearGuiasRemisionFechas.class);
	private ConfService confService;
    private DataSource dataSourceOpenbravo;
	
	public MonitorearGuiasRemisionFechas(DataSource dataSourceOpenbravo){
		this.confService = new ConfService();
        this.dataSourceOpenbravo = dataSourceOpenbravo;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Monitoreando Fechas de Guias de Remision");
		OperacionesBDDHome home = new OperacionesBDDHome();
		Connection connection = null;
		
		try {
			connection = home.getConneccion(dataSourceOpenbravo);
			if(connection != null){
				home.saveLogServicio(connection, "Monitoreando guias de despacho");
				ServiceGuiaRemisionFecha serviceFecha = new ServiceGuiaRemisionFecha(this.confService);
				List<String> auxFechas = serviceFecha.callFechas();
	            serviceFecha = null;
	            
	            Collections.sort(auxFechas);
	            Collections.reverse(auxFechas);
	            
	            if(auxFechas.size() > 30){
	            	List<String> auxFechas2 = auxFechas;
		        	auxFechas = new ArrayList<String>();
		        	for(int i=0;i<30;i++){
		        		auxFechas.add(auxFechas2.get(i));
		        	}

		        	auxFechas2 = null;
	            }
	            
	            for(String fecha: auxFechas){
	            	/***Codigo auxiliar*/
	            	ServiceGuiaRemisionFecha serviceFecha2 = new ServiceGuiaRemisionFecha(this.confService);
	            	List<String> auxFechas2 = serviceFecha2.callFechas();
	            	Collections.sort(auxFechas2);
	                Collections.reverse(auxFechas2);
	                String fechainicial = fecha;
	                
	                if(!auxFechas2.isEmpty()){
	                	fechainicial = auxFechas2.get(0);
	                	auxFechas2 = null;
	                }
	                
	                /**Codigo Auxiliar**/
	                ServiceNroPaginasGuiasRemision serviceNroPaginasGuiasRemision = new ServiceNroPaginasGuiasRemision(this.confService, fecha);
	                int numeroPaginas = serviceNroPaginasGuiasRemision.callDocumentos();
	                log.info("Monitoreando Fecha Guia de Remision: " + fecha);
	            	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
	                
	            	for(int i=1; i <= numeroPaginas; i++){
	            		log.info("Pagina: " + i);
	            		ServiceGuiaRemisionFechaPagina serviceGuiaRemisionFechaPagina = new ServiceGuiaRemisionFechaPagina(this.confService, fecha, String.valueOf(i));
	            		List<ServDocumentoNroNroEsNroEm> auxDoc = serviceGuiaRemisionFechaPagina.callDocumentos();
	            		log.info("Documento por pagina: " + auxDoc.size());
	            		
	            		/**Codigo Auxiliar**/
	            		ServiceNroPaginasGuiasRemision serviceNroPaginasGuiasRemision2 = new ServiceNroPaginasGuiasRemision(this.confService, fechainicial);
	            		int numeroPaginas2 = serviceNroPaginasGuiasRemision2.callDocumentos();
	            		
	            		for(int k=1; k <= numeroPaginas2; k++){
	            			ServiceGuiaRemisionFechaPagina serviceGuiaRemisionFechaPagina2 = new ServiceGuiaRemisionFechaPagina(this.confService, fechainicial, String.valueOf(k));
	            			List<ServDocumentoNroNroEsNroEm>  auxDoc2 = serviceGuiaRemisionFechaPagina2.callDocumentos();
	            			
	            			if(!auxDoc2.isEmpty()){
	            				auxDoc.addAll(auxDoc2);
	            			}
	            		}
	            		
	            		/**Codigo Auxiliar**/
	            		for(ServDocumentoNroNroEsNroEm datodoc: auxDoc){
	            			BDDDocumentoBase documentoBase = new BDDDocumentoBase(
	            					datodoc.getNroDocumento(), 
	            					datodoc.getNroEstablecimiento(), 
	            					datodoc.getNroEmision(), 
	            					datodoc.getTipoDoc(), 
	            					datodoc.getFechadocumento());
	            			home.setDocumentoBase(documentoBase);
	            			
	            			if(home.guardarDocumento(connection)){
                    			home.saveLogServicio(connection, "Preparando documento guia de desapacho de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
                    			log.info("Ingresando documento guia de despacho de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento());
                    			connection.commit();
                    		}else{
                    			connection.rollback();
                    		}
	            		}
	            		auxDoc = null;
	            	}
	            }
	            auxFechas = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (connection != null) connection.close(); connection = null;} catch (Exception ex) {};
		}
	}
}
