package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.dao.servicios.ServicioMonitorearNotasCreditoMigracion;
import atrums.dao.servicios.ServicioMonitorearNotasCreditoSRI;
import atrums.dao.servicios.ServicioMonitorearNotasCreditoWeb;
import atrums.modelo.VariablesServicio;

public class MonitorearNotasCredito implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearNotasCredito.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearNotasCredito(DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Iniciando Servicio de monitoreo Notas de Credito");
		
		try {Thread.sleep(12);} catch (InterruptedException e) {}
		ExecutorService exServiceWeb = Executors.newFixedThreadPool(1);
        Runnable runnableWeb = new ServicioMonitorearNotasCreditoWeb(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceWeb.execute(runnableWeb);
		
        try {Thread.sleep(60015);} catch (InterruptedException e) {}
		ExecutorService exServiceSRI = Executors.newFixedThreadPool(1);
        Runnable runnableServiceSRI = new ServicioMonitorearNotasCreditoSRI(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceSRI.execute(runnableServiceSRI);
        
        try {Thread.sleep(60018);} catch (InterruptedException e) {}
        ExecutorService exServiceMigr = Executors.newFixedThreadPool(1);
        Runnable runnableMigracion = new ServicioMonitorearNotasCreditoMigracion(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exServiceMigr.execute(runnableMigracion);
		
        exServiceMigr.shutdown();
        exServiceSRI.shutdown();
        exServiceWeb.shutdown();
	}
}
