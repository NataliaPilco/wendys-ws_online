package atrums.dao.firma;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.FirmaXML;

public abstract class GenericXMLSignature {
	static final Logger log = Logger.getLogger(GenericXMLSignature.class);
	private String pathSignature;
	private String passSignature;
	private Document docSigned;

	protected void execute() {
		KeyStore kstStore;
		try {
			kstStore = getKeyStore();
		    if (kstStore != null) {
		    	String alias = getAlias(kstStore);
		    	if(alias != null){
			        X509Certificate xctCertificado = null;

			        xctCertificado = (X509Certificate) kstStore.getCertificate(alias);
			        if (xctCertificado != null) {
			        	PrivateKey prkClavePrivada = null;
			        	KeyStore kstTemp = kstStore;
			        	prkClavePrivada = (PrivateKey) kstTemp.getKey(alias, this.passSignature.toCharArray());
			        	Provider prdProvi = kstStore.getProvider();
			        	DataToSign dtsDatos = createDataToSign();
			        	FirmaXML frxml = new FirmaXML();
			        	Object[] res = frxml.signFile(xctCertificado, dtsDatos, prkClavePrivada, prdProvi);
			        	setDocSigned((Document) res[0]);
			        }
		    	}
		      }
		} catch (KeyStoreException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (NoSuchAlgorithmException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (CertificateException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (FileNotFoundException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (UnrecoverableKeyException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (ParserConfigurationException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (SAXException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
  }

  protected abstract DataToSign createDataToSign();

  private KeyStore getKeyStore() {
    KeyStore kstKeys = null;
    try {
		kstKeys = KeyStore.getInstance("PKCS12");
		kstKeys.load(new FileInputStream(pathSignature), passSignature.toCharArray());
	} catch (KeyStoreException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	} catch (NoSuchAlgorithmException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	} catch (CertificateException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	} catch (FileNotFoundException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	} catch (IOException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	}

    return kstKeys;
  }

  private static String getAlias(KeyStore kstStore) {
    String alias = null;
    
	try {
		Enumeration<?> entNombres;
		entNombres = kstStore.aliases();
		while (entNombres.hasMoreElements()) {
			String tmpAlias = (String) entNombres.nextElement();
			
			if (kstStore.isKeyEntry(tmpAlias)) alias = tmpAlias;
		}
	} catch (KeyStoreException ex) {
		// TODO Auto-generated catch block
		log.error(ex.getMessage());
	}

    return alias;
  }

  protected Document getDocument(File flXmldocumento) {
	  	Document docDoc = null;
		
	  	try {
		    DocumentBuilderFactory dbfDoc = DocumentBuilderFactory.newInstance();
		    dbfDoc.setNamespaceAware(true);
		    File file = new File(flXmldocumento.getPath());
		    DocumentBuilder db = dbfDoc.newDocumentBuilder();
			docDoc = db.parse(file);
		} catch (ParserConfigurationException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (SAXException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return docDoc;
  }

  public String getPathSignature() {
    return pathSignature;
  }

  public void setPathSignature(String pathSignature) {
    this.pathSignature = pathSignature;
  }

  public String getPassSignature() {
    return passSignature;
  }

  public void setPassSignature(String passSignature) {
    this.passSignature = passSignature;
  }

  public Document getDocSigned() {
    return docSigned;
  }

  public void setDocSigned(Document docSigned) {
    this.docSigned = docSigned;
  }
}
