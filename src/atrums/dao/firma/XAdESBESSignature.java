package atrums.dao.firma;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import es.mityc.firmaJava.libreria.xades.DataToSign;
import es.mityc.firmaJava.libreria.xades.XAdESSchemas;
import es.mityc.javasign.EnumFormatoFirma;
import es.mityc.javasign.xml.refs.InternObjectToSign;
import es.mityc.javasign.xml.refs.ObjectToSign;

public class XAdESBESSignature extends GenericXMLSignature {
	static final Logger log = Logger.getLogger(XAdESBESSignature.class);
	private File file;
	
	public XAdESBESSignature(File file) {
		this.file = file;
	}
	
	public static File firmar(File flDocumento, String strPathsignature, String strPassSignature) {
		XAdESBESSignature signature = new XAdESBESSignature(flDocumento);
	    signature.setPassSignature(strPassSignature);
	    signature.setPathSignature(strPathsignature);
	    signature.execute();

	    Document docRe = signature.getDocSigned();

	    File file = null;
		try {
			if(docRe != null){
				log.info("Firmando documento");
				file = File.createTempFile("documento", ".xml", null);
				file.deleteOnExit();

			    DOMSource source = new DOMSource(docRe);
			    StreamResult result = new StreamResult(file);

			    TransformerFactory transformerFactory = TransformerFactory.newInstance();
			    Transformer transformer = transformerFactory.newTransformer();
			    transformer.setOutputProperty(OutputKeys.INDENT, "no");
			    transformer.transform(source, result);
			}
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (TransformerConfigurationException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (TransformerException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	    
		return file;
	}
	
	@Override
	protected DataToSign createDataToSign() {
		// TODO Auto-generated method stub
		DataToSign documentoAFirmar = new DataToSign();
		documentoAFirmar.setXadesFormat(EnumFormatoFirma.XAdES_BES);
		documentoAFirmar.setEsquema(XAdESSchemas.XAdES_132);
		documentoAFirmar.setXMLEncoding("UTF-8");
		documentoAFirmar.setEnveloped(true);

		documentoAFirmar.addObject(new ObjectToSign(new InternObjectToSign("comprobante"),"contenido comprobante", null, "text/xml", null));
		documentoAFirmar.setParentSignNode("comprobante");

		Document docToSign = getDocument(file);
		documentoAFirmar.setDocument(docToSign);
		  
		return documentoAFirmar;
	}
}
