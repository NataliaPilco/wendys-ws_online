package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceNotaCreditoFecha;
import atrums.persistencia.ServiceNotaCreditoPorFechaPagina;
import atrums.persistencia.ServiceNroPaginasNotas;

public class MonitorearNotasCreditoFechas implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearNotasCreditoFechas.class);
    private ConfService confService;
	private DataSource dataSourceOpenbravo;
    
    public MonitorearNotasCreditoFechas(DataSource dataSourceOpenbravo) {
    	this.confService = new ConfService();
        this.dataSourceOpenbravo = dataSourceOpenbravo;
	}
    
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Monitoreando Fechas de Notas de Credito");
		OperacionesBDDHome home = new OperacionesBDDHome(null);
    	Connection connection = null;
    	connection = home.getConneccion(dataSourceOpenbravo);
		
    	try{
    		if(connection != null){
    			home.saveLogServicio(connection, "Monitoreando notas de credito");
    			ServiceNotaCreditoFecha serviceNotaCreditoFecha = new ServiceNotaCreditoFecha(confService);
    	        List<String> auxFechas = serviceNotaCreditoFecha.callFechas();
    	        Collections.sort(auxFechas);
    	        Collections.reverse(auxFechas);
    	        
    	        if(auxFechas.size() > 10){
    	        	List<String> auxFechas2 = auxFechas;
    	        	auxFechas = new ArrayList<String>();
    	        	for(int i=0;i<10;i++){
    	        		auxFechas.add(auxFechas2.get(i));
    	        	}
    	        	
    	        	auxFechas2 = null;
    	        }
    	        
    	        //Collections.reverse(auxFechas);
    	        
    	        for(String fecha: auxFechas){
    	        	/**Codigo Auxiliar**/
    	        	ServiceNotaCreditoFecha serviceNotaCreditoFecha2 = new ServiceNotaCreditoFecha(confService);
                    List<String> auxFechas2 = serviceNotaCreditoFecha2.callFechas();
                    Collections.sort(auxFechas2);
                    Collections.reverse(auxFechas2);
                    String fechainicial = fecha;
                    
                    if(!auxFechas2.isEmpty()){
                    	fechainicial = auxFechas2.get(0);
                    	auxFechas2 = null;
                    }
                    /**Codigo Auxiliar**/
    	        	
    	        	ServiceNroPaginasNotas serviceNroPaginasNotas = new ServiceNroPaginasNotas(confService, fecha);
    	        	int numeroPaginas = serviceNroPaginasNotas.callDocumentos();
    	        	log.info("Monitoreando Fecha Nota de Credito: " + fecha);
    	        	log.info("Paginas de la Fecha: " + fecha + " - " + numeroPaginas);
    	        	
    	        	for(int i=1; i <= numeroPaginas; i++){
    	        		ServiceNotaCreditoPorFechaPagina serviceNotaCreditoPorFechaPagina = new ServiceNotaCreditoPorFechaPagina(confService, fecha, String.valueOf(i));
    	        		List<ServDocumentoNroNroEsNroEm> auxDoc = serviceNotaCreditoPorFechaPagina.callDocumentos();
    	        		
    	        		/**Codigo Auxiliar**/
    	        		ServiceNroPaginasNotas serviceNroPaginasNotas2 = new ServiceNroPaginasNotas(confService, fechainicial);
                    	int numeroPaginas2 = serviceNroPaginasNotas2.callDocumentos();
                    	
                    	for(int k=1; k <= numeroPaginas2; k++){
                    		ServiceNotaCreditoPorFechaPagina serviceNotaCreditoPorFechaPagina2 = new ServiceNotaCreditoPorFechaPagina(confService, fechainicial, String.valueOf(k));
                    		List<ServDocumentoNroNroEsNroEm> auxDoc2 = serviceNotaCreditoPorFechaPagina2.callDocumentos();
                    		
                    		if(!auxDoc2.isEmpty()){
                    			auxDoc.addAll(auxDoc2);
                    		}
                    	}
                    	/**Codigo Auxiliar**/
    	        		
    	        		for(ServDocumentoNroNroEsNroEm datodoc: auxDoc){
    	        			BDDDocumentoBase documentoBase = new BDDDocumentoBase(
	                    			datodoc.getNroDocumento(), 
	                    			datodoc.getNroEstablecimiento(), 
	                    			datodoc.getNroEmision(), 
	                    			datodoc.getTipoDoc(), 
	                    			datodoc.getFechadocumento());
	                    	home.setDocumentoBase(documentoBase);
	                    	
	                    	try{
                        		if(home.guardarDocumento(connection)){
                        			home.saveLogServicio(connection, "Preparando documento nota de credito de la fecha: " + fecha 
                        					+ " - documento: " + documentoBase.getNrodocumento() 
                        					+ " - ptoemision: " + documentoBase.getNroemision()
                        					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
                        			log.info("Ingresando documento nota de credito de la fecha: " + fecha 
                        					+ " - documento: " + documentoBase.getNrodocumento() 
                        					+ " - ptoemision: " + documentoBase.getNroemision()
                        					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento());
                        			connection.commit();
                        		}else{
                        			connection.rollback();
                        		}
                        	} catch (SQLException ex) {
                        		connection = home.getConneccion(dataSourceOpenbravo);
                        		log.error(ex.getMessage(), ex);
            				}
    	            	}
    	        		auxDoc = null;
    	        	}
    	        }
                auxFechas = null;
    		}
    	} catch (Exception ex){
    		log.error(ex.getMessage(), ex);
		} finally {
    		try { if (connection != null) connection.close(); connection = null;} catch (Exception ex) {};
    	}
	}
}