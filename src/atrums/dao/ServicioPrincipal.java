package atrums.dao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import atrums.modelo.VariablesServicio;
import atrums.persistencia.OperacionConexion;

public class ServicioPrincipal implements Runnable{
	static final Logger log = Logger.getLogger(ServicioPrincipal.class);
	private DataSource dataSourceOpenbravo = null;
	
	private VariablesServicio variablesServicio = new VariablesServicio(); 
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Iniciando Servicio Completo de migración");
		
		/**60000 un minuto**/
		//try {log.info("Preparando Ambiente");Thread.sleep(360000);} catch (InterruptedException e) {}
    	
		try {log.info("Preparando Ambiente");Thread.sleep(60000);} catch (InterruptedException e) {}
		
		this.dataSourceOpenbravo = OperacionConexion.getDataSource();
		
        ExecutorService exFacturas = Executors.newFixedThreadPool(1);
        Runnable runnableFacturas = new MonitorearFacturas(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exFacturas.execute(runnableFacturas);
        
        ExecutorService exNotasCredito = Executors.newFixedThreadPool(1);
        Runnable runnableNotasCredito = new MonitorearNotasCredito(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exNotasCredito.execute(runnableNotasCredito);
        
        ExecutorService exClaves = Executors.newFixedThreadPool(1);
    	Runnable runnableClaves = new MonitorearClaves(
    			this.dataSourceOpenbravo, 
    			this.variablesServicio);
    	exClaves.execute(runnableClaves);
        
        ExecutorService exRetencion = Executors.newFixedThreadPool(1);
        Runnable runnableRetencion = new MonitorearRetenciones(
        		this.dataSourceOpenbravo, 
        		this.variablesServicio);
        exRetencion.execute(runnableRetencion);
		
		ExecutorService exGuiaRemision = Executors.newFixedThreadPool(1);
		Runnable runnableGuiaRemision = new MonitoreoGuiasRemision(
				this.dataSourceOpenbravo, 
				this.variablesServicio);
		exGuiaRemision.execute(runnableGuiaRemision);
		
        exFacturas.shutdown();
        exNotasCredito.shutdown();
        exClaves.shutdown();
        exRetencion.shutdown();
		exGuiaRemision.shutdown();
        
        while (!exFacturas.isTerminated()){}
        while (!exNotasCredito.isTerminated()){}
        while (!exClaves.isTerminated()){}
        while (!exRetencion.isTerminated()){}
		while (!exGuiaRemision.isTerminated()){}
	}
}
