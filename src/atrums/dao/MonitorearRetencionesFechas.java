package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.Servicio.ServDocumentoSucNumFNumDocProve;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.ServiceRetencionFecha;
import atrums.persistencia.ServiceRetencionPorFecha;

public class MonitorearRetencionesFechas implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearRetencionesFechas.class);
    private ConfService confService;
    private DataSource dataSourceOpenbravo;
    
    public MonitorearRetencionesFechas(DataSource dataSourceOpenbravo){
        this.confService = new ConfService();
        this.dataSourceOpenbravo = dataSourceOpenbravo;
    }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		log.info("Monitoreando Fechas de Retenciones");
		OperacionesBDDHome home = new OperacionesBDDHome(null);					
    	Connection connection = null;
    	connection = home.getConneccion(dataSourceOpenbravo);
    	
    	try{
    		if(connection != null){
    			home.saveLogServicio(connection, "Monitoreando retenciones");
    			ServiceRetencionFecha serviceRetencionFecha = new ServiceRetencionFecha(confService);
    			List<String> auxFechas = serviceRetencionFecha.callFechas();
    			Collections.sort(auxFechas);
    			for(String fecha: auxFechas){
    				ServiceRetencionPorFecha serviceRetencionPorFecha = new ServiceRetencionPorFecha(confService, fecha);
    				List<ServDocumentoSucNumFNumDocProve> auxDoc = serviceRetencionPorFecha.callDocumentos();
    				for(ServDocumentoSucNumFNumDocProve datodoc: auxDoc){
    					BDDDocumentoBase documentoBase = new BDDDocumentoBase(
								datodoc.getNroDocumento(), 
				        		datodoc.getSucursal(), 
				        		datodoc.getNroFactura(), 
				        		"RT", 
				        		fecha);
    					home.setDocumentoBase(documentoBase);
    					
    					try{
	                		if(home.guardarDocumento(connection)){
	                			home.saveLogServicio(connection, "Preparando documento retencions de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento() + " - para procesamiento");
	                			log.info("Ingresando documento retención de la fecha: " + fecha 
                    					+ " - documento: " + documentoBase.getNrodocumento() 
                    					+ " - ptoemision: " + documentoBase.getNroemision()
                    					+ " - ptoestablecimiento: " + documentoBase.getNroestablecimiento());
	                			connection.commit();
	                		}else{
	                			connection.rollback();
	                		}
	                	} catch (SQLException ex) {
	                		log.error(ex.getMessage());
                    		connection = home.getConneccion(dataSourceOpenbravo);
	    				}
    				}
    				auxDoc = null;
    			}
    			try { if (connection != null) connection.close(); connection = null;} catch (Exception ex) {};
                auxFechas = null;
    		}
    	} catch (Exception ex){
    		log.error(ex.getMessage(), ex);
		} finally {
    		try { if (connection != null) connection.close(); connection = null;} catch (Exception ex) {};
    	}
	}
}