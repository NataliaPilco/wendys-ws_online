package atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import javax.sql.DataSource;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;

import atrums.modelo.ConfBDDOpenbravo;
import atrums.modelo.ConfService;
import atrums.modelo.VariablesServicio;
import atrums.modelo.BDDHome.BDDDocumentoBase;
import atrums.modelo.BDDHome.BDDEmpresaPrincipal;
import atrums.modelo.BDDOpenbravo.BDDOadUserEmail;
import atrums.modelo.BDDOpenbravo.BDDOcInvoiceLine;
import atrums.modelo.BDDOpenbravo.BDDOcTax;
import atrums.modelo.BDDOpenbravo.BDDOcTaxcategory;
import atrums.modelo.BDDOpenbravo.BDDOmProduct;
import atrums.modelo.BDDOpenbravo.BDDOmProductCategory;
import atrums.modelo.SRI.SRIDocumentoAutorizadoClave;
import atrums.modelo.Servicio.ServCliente;
import atrums.modelo.Servicio.ServDireccion;
import atrums.modelo.Servicio.ServEmail;
import atrums.modelo.Servicio.ServRespuestaControl;
import atrums.modelo.documento.DatosInfo;
import atrums.modelo.documento.Detalle;
import atrums.modelo.documento.Detalles;
import atrums.modelo.documento.InfoFactura;
import atrums.modelo.documento.InfoNotaCredito;
import atrums.modelo.documento.InfoTributaria;
import atrums.persistencia.OperacionesBDDHome;
import atrums.persistencia.OperacionesBDDOpenbravo;
import atrums.persistencia.ServiceAutorizacionClaves;
import atrums.persistencia.ServiceCliente;
import atrums.persistencia.ServiceInsertarFEControlDocumentos;

public class MonitorearClavesNotaCreditoMigracion implements Runnable{
	static final Logger log = Logger.getLogger(MonitorearClavesNotaCreditoMigracion.class);
	private DataSource dataSourceOpenbravo;
	private VariablesServicio variablesServicio = null;
	
	public MonitorearClavesNotaCreditoMigracion(
			DataSource dataSourceOpenbravo, 
			VariablesServicio variablesServicio) {
		this.dataSourceOpenbravo = dataSourceOpenbravo;
		this.variablesServicio = variablesServicio;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Connection conHome = null;
		Connection conOpenbravo = null;
		
		try{
			ConfBDDOpenbravo confBDDOpenbravo = new ConfBDDOpenbravo();
			ConfService confService = new ConfService();
			
			BDDDocumentoBase documentoBase = new BDDDocumentoBase();
			OperacionesBDDHome home = new OperacionesBDDHome(documentoBase);
			conHome = home.getConneccion(dataSourceOpenbravo);
			
			if(conHome != null  && this.variablesServicio.isExtDocumentoFacClaveN()){
				if(home.getBDDDocumentoBaseMigrar("NCP", conHome)){
					log.info("Procesando Documento XML Nota de credito: " + documentoBase.getNrodocumento());
					documentoBase = home.getDocumentoBase();
					
					log.info("Migrando Documento XML Nota de credito: " + documentoBase.getNrodocumento());
					
					BDDEmpresaPrincipal empresaPrincipal = new BDDEmpresaPrincipal();
					OperacionesBDDOpenbravo openbravo = new OperacionesBDDOpenbravo(confBDDOpenbravo, 
							empresaPrincipal, 
							confService);
					conOpenbravo = openbravo.getConneccion(dataSourceOpenbravo);
					openbravo.setDocumentoBase(documentoBase);
					
					if(openbravo.getEmpresaPrincipal(conOpenbravo)){
						if(openbravo.existeDocumentoImporFCNC(conOpenbravo)){
							SOAPMessage message = null;
							
							try{
								SRIDocumentoAutorizadoClave autorizado = new SRIDocumentoAutorizadoClave();
								ServiceAutorizacionClaves serviceAutorizacion = new ServiceAutorizacionClaves(confService, 
										documentoBase.getClaveacceso());
								autorizado = serviceAutorizacion.CallAutorizado();
								
								if(autorizado.getDocXML() != null){
									message = autorizado.getDocFile();
								}
							} catch (Exception ex){}
							
							if(message != null){
								String documentooriginal = message.getSOAPBody().getElementsByTagName("comprobante").item(0).getFirstChild().getNodeValue();
								InfoTributaria infoTributaria = new InfoTributaria(documentooriginal);
								InfoFactura infoFactura = new InfoFactura(documentooriginal);
								InfoNotaCredito infoNotaCredito = new InfoNotaCredito(documentooriginal);
								Detalles detalles = new Detalles(documentooriginal);
								
								DatosInfo datosInfo = null;
								Float propina = (float) 0;
								
								if(infoFactura.getIdentificacionComprador() != null){
									datosInfo = new DatosInfo(infoFactura.getIdentificacionComprador(), 
											infoFactura.getFechaEmision(), 
											"", 
											infoFactura.getRazonSocialComprador(), 
											infoFactura.getTipoIdentificacionComprador());
									propina = Float.valueOf(infoFactura.getPropina());
								}else if(infoNotaCredito.getIdentificacionComprador() != null){
									datosInfo = new DatosInfo(infoNotaCredito.getIdentificacionComprador(), 
											infoNotaCredito.getFechaEmision(), 
											"", 
											infoNotaCredito.getRazonSocialComprador(), 
											infoNotaCredito.getTipoIdentificacionComprador());
								}
								
								if(datosInfo != null){
									if(openbravo.getBDDOadClient(conOpenbravo)){
										documentoBase.setNroestablecimiento(infoTributaria.getEstab());
										documentoBase.setNroemision(infoTributaria.getPtoEmi());
										openbravo.setDocumentoBase(documentoBase);
										if(openbravo.getBDDOadOrg(conOpenbravo)){
											documentoBase.setNroestablecimiento("001");
											documentoBase.setNroemision("001");
											openbravo.setDocumentoBase(documentoBase);
											if(openbravo.getBDDOcDoctype(conOpenbravo)){
												boolean continuar = true;
												
												if(openbravo.existeDato("SELECT count(*) AS total FROM c_bpartner as cb WHERE cb.taxid = '" + datosInfo.getIdentificacionComprador() + "';", conOpenbravo)){
													ServiceCliente serviceCliente = new ServiceCliente(confService, datosInfo.getIdentificacionComprador());
													ServCliente cliente = serviceCliente.callCliente();
													
													if(cliente.getIdentificacion() == null){
														cliente.setIdentificacion(datosInfo.getIdentificacionComprador());
														cliente.setNombreComercial(datosInfo.getRazonSocial());
														cliente.setRazonSocial(datosInfo.getRazonSocial());
														cliente.setTipo(datosInfo.getTipo());
														cliente.setDireccion(new ServDireccion());
														cliente.setEmail(new ServEmail());
													}
													
													if(cliente.getIdentificacion() != null){
														
														if(cliente.getEmail().getEmail() == null){
															ServEmail auxEmail = new ServEmail();
															auxEmail.setIdentificacion(cliente.getIdentificacion());
															auxEmail.setEmail("N/A");
															cliente.setEmail(auxEmail);
														}
														
														if(cliente.getDireccion().getDireccion() == null){
															ServDireccion auxDireccion = new ServDireccion();
															auxDireccion.setIdentificacion(cliente.getIdentificacion());
															auxDireccion.setDireccion("N/A");
															auxDireccion.setDireccion2("N/A");
															cliente.setDireccion(auxDireccion);
														}
														
														openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
														openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
														openbravo.getOcbPartner().setTipo(cliente.getTipo());
														openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
														openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
														
														
														if(openbravo.existeDato("SELECT count(*) AS total FROM c_bp_group as cb WHERE upper(cb.name) LIKE '%CLIENTES COMERCIALES%';", conOpenbravo)){
															if(!openbravo.saveBDDOcbGroup(conOpenbravo)){
																conOpenbravo.rollback();
																continuar = false;
															}
														}
														
														if(openbravo.saveBDDOcbPartner(conOpenbravo) && continuar){
															openbravo.getOadUserEmail().setEmail(cliente.getEmail().getEmail());
															if(openbravo.saveBDDOadUser(conOpenbravo)){
																openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
																if(openbravo.saveBDDOcLocation(conOpenbravo)){
																	if(openbravo.saveBDDOcbPartnerLocation(conOpenbravo)){
																		openbravo.setOadUserEmail(new BDDOadUserEmail());
																		if(openbravo.saveBDDOadUserPortal(conOpenbravo)){
																			if(openbravo.existeDato("SELECT count(*) AS total FROM ad_role WHERE upper(name) like '%CONSULTOR DOCUMENTOS ELECTRÓNICOS%'", conOpenbravo)){
																				if(openbravo.saveBDDOadRole(conOpenbravo)){
																					if(openbravo.saveBDDOadRoleOrgaccess(conOpenbravo)){
																						if(!openbravo.saveBDDOadWindowAccess(conOpenbravo)){
																							conOpenbravo.rollback();
																							continuar = false;
																						}
																					}else{
																						conOpenbravo.rollback();
																						continuar = false;
																					}
																				}else{
																					conOpenbravo.rollback();
																					continuar = false;
																				}
																			}
																			
																			if(!openbravo.saveBDDOadUserRoles(conOpenbravo)){
																				conOpenbravo.rollback();
																				continuar = false;
																			}
																			
																		}else{
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}else{
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																}else{
																	conOpenbravo.rollback();
																	continuar = false;
																}
															}else{
																conOpenbravo.rollback();
																continuar = false;
															}
														}else{
															conOpenbravo.rollback();
															continuar = false;
														}
													}
												}else{
													openbravo.getOcbPartner().setIdentificacion(datosInfo.getIdentificacionComprador());
													
													ServiceCliente serviceCliente = new ServiceCliente(confService, datosInfo.getIdentificacionComprador());
													ServCliente cliente = serviceCliente.callCliente();
													
													if(cliente.getIdentificacion() == null){
														cliente.setIdentificacion(datosInfo.getIdentificacionComprador());
														cliente.setNombreComercial(datosInfo.getRazonSocial());
														cliente.setRazonSocial(datosInfo.getRazonSocial());
														cliente.setTipo(datosInfo.getTipo());
														cliente.setDireccion(new ServDireccion());
														cliente.setEmail(new ServEmail());
													}
													
													if(cliente.getIdentificacion() != null){
														
														if(cliente.getEmail().getEmail() == null){
															ServEmail auxEmail = new ServEmail();
															auxEmail.setIdentificacion(cliente.getIdentificacion());
															auxEmail.setEmail("N/A");
															cliente.setEmail(auxEmail);
														}
														
														if(cliente.getDireccion().getDireccion() == null){
															ServDireccion auxDireccion = new ServDireccion();
															auxDireccion.setIdentificacion(cliente.getIdentificacion());
															auxDireccion.setDireccion("N/A");
															auxDireccion.setDireccion2("N/A");
															cliente.setDireccion(auxDireccion);
														}
														
														openbravo.getOcbPartner().setNombreComercial(cliente.getNombreComercial());
														openbravo.getOcbPartner().setRazonSocial(cliente.getRazonSocial());
														openbravo.getOcbPartner().setTipo(cliente.getTipo());
														openbravo.getOcbPartner().setIdentificacion(cliente.getIdentificacion());
														openbravo.getOcbPartner().setEmail(cliente.getEmail().getEmail());
													}
													
													openbravo.getOcLocation().setDireccion(cliente.getDireccion().getDireccion());
													openbravo.getOadUserEmail().setEmail(cliente.getEmail().getEmail());
													
													if(openbravo.getBDDOcbPartner(conOpenbravo)){
														if(!openbravo.getBDDOadUser(conOpenbravo)){
															conOpenbravo.rollback();
															continuar = false;
														}
													}else{
														conOpenbravo.rollback();
														continuar = false;
													}
												}
												
												if(openbravo.getOcbPartner().getId() != null && continuar){
													try{
														SRIDocumentoAutorizadoClave autorizado = new SRIDocumentoAutorizadoClave();
														ServiceAutorizacionClaves serviceAutorizacion = new ServiceAutorizacionClaves(confService, 
																documentoBase.getClaveacceso());
														autorizado = serviceAutorizacion.CallAutorizado();
														
														if(autorizado.getDocXML() != null){
															openbravo.getOcInvoice().setFactura(autorizado.getDocXML());
															documentoBase.setNroautorizacion(autorizado.getNumeroAutorizacion());
															documentoBase.setFechaautorizacion(autorizado.getFechaAutorizacion());
														}
													} catch (Exception ex){}
													
													openbravo.getOcInvoice().setNroDocumento(documentoBase.getNrodocumento());
													openbravo.getOcInvoice().setFechaEmision(documentoBase.getFechadocumento());
													openbravo.getOcInvoice().setFechaContable(documentoBase.getFechadocumento());
													openbravo.getOcInvoice().setNumeroAutorizacion(documentoBase.getNroautorizacion());
													openbravo.getOcInvoice().setFechaAutorizacion(documentoBase.getFechaautorizacion());
													openbravo.getOcInvoice().setNroEstablecimiento(infoTributaria.getEstab());
													openbravo.getOcInvoice().setNroEmision(infoTributaria.getPtoEmi());
													openbravo.getOcInvoice().setEstado("AUTORIZADO");
													openbravo.getOcInvoice().setClaveAcceso(documentoBase.getClaveacceso());
													openbravo.getOcInvoice().setTipoDocumento(documentoBase.getTipodocumento());
													openbravo.getOcInvoice().setDescripcion("Basado en factura clientes (" + infoNotaCredito.getNumDocModificado() + ")");
													
													if(openbravo.existeDato("SELECT count(*) AS total FROM c_paymentterm AS cp WHERE upper(cp.name) like '%CONTADO%'", conOpenbravo)){
														if(!openbravo.saveBDDOcPaymentterm(conOpenbravo)){
															conOpenbravo.rollback();
															continuar = false;
														}
													}
													
													if(openbravo.existeDato("SELECT count(*) AS total FROM m_pricelist AS mp WHERE upper(mp.name) LIKE '%VENTA%'", conOpenbravo)){
														if(!openbravo.saveBDDOmPricelist(conOpenbravo)){
															conOpenbravo.rollback();
															continuar = false;
														}
													}
													
													if(continuar){
														if(openbravo.saveBDDOcInvoice(conOpenbravo)){
															int linea = 10;
															List<Detalle> lineas = detalles.getDetalles();
															
															for(int i=0; i< lineas.size(); i++){
																
																if(continuar){
																	openbravo.setOmProductCategory(new BDDOmProductCategory());
																	if(openbravo.existeDato("SELECT count(*) AS total FROM m_product_category AS mp WHERE upper(mp.name) LIKE '%" + openbravo.getOmProductCategory().getName().toUpperCase() + "%'", conOpenbravo)){
																		if(!openbravo.saveBDDmProductCategory(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOcTaxcategory(new BDDOcTaxcategory());
																	
																	String impuestoCodigo = null;
																	if(lineas.get(i).getImpuestos().get(0).getCodigoPorcentaje().equals("0")){
																		impuestoCodigo = "0";
																	}else if(lineas.get(i).getImpuestos().get(0).getCodigoPorcentaje().equals("2")){
																		impuestoCodigo = "12";
																	}else if(lineas.get(i).getImpuestos().get(0).getCodigoPorcentaje().equals("4")){
																		impuestoCodigo = "14";
																	}else{
																		impuestoCodigo = "14";
																	}
																	
																	openbravo.getOcTaxcategory().setNombre("IVA " + impuestoCodigo);
																	
																	if(Integer.valueOf(Double.valueOf(impuestoCodigo).intValue()) == 0){
																		openbravo.getOcTaxcategory().setCodigo("0");
																	}else if(Integer.valueOf(Double.valueOf(impuestoCodigo).intValue()) == 12){
																		openbravo.getOcTaxcategory().setCodigo("2");
																	}else if(Integer.valueOf(Double.valueOf(impuestoCodigo).intValue()) == 14){
																		openbravo.getOcTaxcategory().setCodigo("4");
																	}
																	
																	if(openbravo.existeDato("SELECT count(*) AS total FROM c_taxcategory as ct WHERE upper(ct.name) like '" + openbravo.getOcTaxcategory().getNombre() + "' AND ct.em_co_tp_base_imponible = '" + openbravo.getOcTaxcategory().getCodigo() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOcTaxcategory(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOcTax(new BDDOcTax());
																	openbravo.getOcTax().setRate(lineas.get(i).getImpuestos().get(0).getTarifa());
																	
																	if(openbravo.existeDato("SELECT count(*) AS total FROM c_tax WHERE upper(name) like '" + openbravo.getOcTaxcategory().getNombre() + "' AND rate = '" + lineas.get(i).getImpuestos().get(0).getTarifa() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOcTax(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}
																	
																	openbravo.setOmProduct(new BDDOmProduct());
																	String codProducto = ""; 
																	
																	if(lineas.get(i).getCodigoInterno().replace(" ", "").toLowerCase().length() > 25){
																		codProducto = lineas.get(i).getCodigoInterno().replace(" ", "").replace("'", "").replace(",", "").toLowerCase().substring(0, 25);
																	}else{
																		codProducto = lineas.get(i).getCodigoInterno().replace(" ", "").replace("'", "").replace(",", "").toLowerCase();
																	}
																	
																	openbravo.getOmProduct().setCodigo(codProducto);
																	openbravo.getOmProduct().setName(lineas.get(i).getDescripcion().replace("'", "").replace(",", ""));
																	if(openbravo.existeDato("SELECT count(*) AS total FROM m_product AS mp WHERE mp.value = '" + openbravo.getOmProduct().getCodigo() + "'", conOpenbravo)){
																		if(!openbravo.saveBDDOmProduct(conOpenbravo)){
																			conOpenbravo.rollback();
																			continuar = false;
																		}
																	}else if(!openbravo.updateBDDOmProduct(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																	
																	DecimalFormat format2 = new DecimalFormat("0.00");
																	openbravo.setOcInvoiceLine(new BDDOcInvoiceLine());
																	openbravo.getOcInvoiceLine().setLinea(String.valueOf(linea));
																	openbravo.getOcInvoiceLine().setCantidad(lineas.get(i).getCantidad());
																	openbravo.getOcInvoiceLine().setPrecioUnitario(format2.format(Double.valueOf(lineas.get(i).getPrecioUnitario())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setDescuento(format2.format(Double.valueOf(lineas.get(i).getDescuento())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setPrecioTotal(format2.format(Double.valueOf(lineas.get(i).getPrecioTotalSinImpuesto())).replace(",", "."));
																	openbravo.getOcInvoiceLine().setValor(format2.format(Double.valueOf(lineas.get(i).getImpuestos().get(0).getValor())).replace(",", "."));
																	
																	if(!openbravo.saveBDDOcInvoiceLine(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}else{
																		linea = linea + 10;
																	}
																}
															}
															
															if(propina > 0 && continuar){
																openbravo.setOcTaxcategory(new BDDOcTaxcategory());
																
																openbravo.getOcTaxcategory().setNombre("EXCENTO");
																openbravo.getOcTaxcategory().setCodigo("2");
																
																if(openbravo.existeDato("SELECT count(*) AS total FROM c_taxcategory as ct WHERE upper(ct.name) like '%" + openbravo.getOcTaxcategory().getNombre() + "%' AND ct.em_co_tp_base_imponible = '" + openbravo.getOcTaxcategory().getCodigo() + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOcTaxcategory(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																}
																
																openbravo.setOcTax(new BDDOcTax());
																openbravo.getOcTax().setRate("0");
																
																if(openbravo.existeDato("SELECT count(*) AS total FROM c_tax WHERE upper(name) like '%" + openbravo.getOcTaxcategory().getNombre() + "%' AND rate = '" + "0" + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOcTax(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																}
																
																openbravo.setOmProduct(new BDDOmProduct());
																String codProducto = "propina"; 
																
																openbravo.getOmProduct().setCodigo(codProducto);
																openbravo.getOmProduct().setName("Propina");
																if(openbravo.existeDato("SELECT count(*) AS total FROM m_product AS mp WHERE mp.value = '" + openbravo.getOmProduct().getCodigo() + "'", conOpenbravo)){
																	if(!openbravo.saveBDDOmProduct(conOpenbravo)){
																		conOpenbravo.rollback();
																		continuar = false;
																	}
																}
																
																DecimalFormat format2 = new DecimalFormat("0.00");
																openbravo.setOcInvoiceLine(new BDDOcInvoiceLine());
																openbravo.getOcInvoiceLine().setLinea(String.valueOf(linea));
																openbravo.getOcInvoiceLine().setCantidad("1");
																openbravo.getOcInvoiceLine().setPrecioUnitario(format2.format(Double.valueOf(propina)).replace(",", "."));
																openbravo.getOcInvoiceLine().setDescuento(format2.format(Double.valueOf("0")).replace(",", "."));
																openbravo.getOcInvoiceLine().setPrecioTotal(format2.format(Double.valueOf(propina)).replace(",", "."));
																openbravo.getOcInvoiceLine().setValor(format2.format(Double.valueOf("0")).replace(",", "."));
																
																if(!openbravo.saveBDDOcInvoiceLine(conOpenbravo)){
																	conOpenbravo.rollback();
																	continuar = false;
																}
															}
															
															if(continuar){
																ServiceInsertarFEControlDocumentos controlDocumentos = new ServiceInsertarFEControlDocumentos(
																		confService, 
																		documentoBase.getNrodocumento(), 
																		infoTributaria.getEstab(), 
																		infoTributaria.getPtoEmi(), 
																		documentoBase.getNroautorizacion(), 
																		documentoBase.getFechaautorizacion(), 
																		documentoBase.getFechadocumento(), 
																		"2", 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		openbravo.getOcbPartner().getIdentificacion(), 
																		documentoBase.getTipodocumento().substring(0, 2));
																
																ServRespuestaControl respuestaControl = controlDocumentos.callRespuesta();
																
																if(respuestaControl.isRespuesta()){
																	conOpenbravo.commit();
																	log.info("Datos migrados");
																	
																	documentoBase.setEstado("MIG");
																	documentoBase.setMensaje("FINAL / Documento migrado a los dos ambientes");
																	documentoBase.setUsuario(openbravo.getOcbPartner().getIdentificacion());
																	documentoBase.setPassword(openbravo.getOcbPartner().getIdentificacion());
																	if(home.actualizarEstado(conHome)){
																		conHome.commit();
																	}else{
																		conHome.rollback();
																	}
																}else{
																	conOpenbravo.rollback();
																	log.info("Datos no migrados ver log");
																}
															}
														}else{
															if(home.actualizarEstado(conHome)){
																conHome.commit();
															}else{
																conHome.rollback();
															}
															conOpenbravo.rollback();
															continuar = false;
														}
													}
												}
											}else{
												log.info("Esperando documento...");
												documentoBase.setMensaje("ERROR / No hay una documento nota de credito para la sucursal con establecimiento: " + infoTributaria.getEstab() + ", emision: " + infoTributaria.getPtoEmi());
												home.setDocumentoBase(documentoBase);
												if(home.actualizarEstado(conHome)){
													conHome.commit();
												}else{
													conHome.rollback();
												}
											}
										}else{
											log.info("Esperando sucursal...");
											documentoBase.setMensaje("ERROR / No hay una sucursal para este documento, establecimiento: " + infoTributaria.getEstab() + ", emision: " + infoTributaria.getPtoEmi());
											home.setDocumentoBase(documentoBase);
											if(home.actualizarEstado(conHome)){
												conHome.commit();
											}else{
												conHome.rollback();
											}
										}
									}else{
										log.info("Esperando empresa...");
										documentoBase.setMensaje("ERROR / No hay una empresa para este documento");
										home.setDocumentoBase(documentoBase);
										if(home.actualizarEstado(conHome)){
											conHome.commit();
										}else{
											conHome.rollback();
										}
									}
								}
							}else{
								documentoBase.setMensaje("ERROR / No existe el documento en el SRI,  clave de acceso provista por el cliente: " + documentoBase.getClaveacceso());
								home.setDocumentoBase(documentoBase);
								if(home.actualizarEstado(conHome)){
									conHome.commit();
								}else{
									conHome.rollback();
								}
							}
						}else{
							documentoBase.setEstado("MIG");
							home.setDocumentoBase(documentoBase);
							if(home.actualizarEstado(conHome)){
								conHome.commit();
							}else{
								conHome.rollback();
							}
						}							
					}else{
						log.info("Esperando empresa...");
						try { if (conOpenbravo != null) conOpenbravo.close(); } catch (Exception ex) {};
						try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
						this.variablesServicio.setExtDocumentoFacClaveN(false);
					}
				}else{
					log.info("Esperando documento...");
					try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
					this.variablesServicio.setExtDocumentoFacClaveN(false);
				}
			}else{
				log.info("Esperando conexión...");
				this.variablesServicio.setExtDocumentoFacClaveN(false);
			}
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} catch (DOMException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage(), ex);
		} finally {
			try { if (conOpenbravo != null) conOpenbravo.close(); } catch (Exception ex) {};
			try { if (conHome != null) conHome.close(); } catch (Exception ex) {};
		}
	}
}
