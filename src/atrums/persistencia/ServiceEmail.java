package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServEmail;

public class ServiceEmail {
	static final Logger log = Logger.getLogger(ServiceEmail.class);
    private final ConfService confService;
    private String identificacion;
    
    public ServiceEmail(ConfService confService, String identificacion){
        this.confService = confService;
        this.identificacion = identificacion;
    }
    
    public ServEmail callEmail(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("identificacion", this.identificacion);
    	
        ServicioWendysCall wendysCall = new ServicioWendysCall(confService.getwSDL(), 
        		auxParametros, 
                confService.getConsultarCorreoClientePorIdentificacion());
        
        ServEmail email = new ServEmail(wendysCall.Call());
        
        return email;
    }
}
