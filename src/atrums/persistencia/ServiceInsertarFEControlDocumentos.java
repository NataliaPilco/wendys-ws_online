package atrums.persistencia;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServRespuestaControl;

public class ServiceInsertarFEControlDocumentos {
	static final Logger log = Logger.getLogger(ServiceInsertarFEControlDocumentos.class);
	private ConfService confService;
	private String numDocumento = null;
	private String ptoEstablecimiento = null;
	private String ptoEmision = null;
	private String numAutorizaSri = null;
	private String fechaAutoriza = null;
	private String fechaDocumento = null;
	private String estado = null;
	private String usuario = null;
	private String clave = null;
	private String tipo = null;
	private String ultimoCambio = null;
	
	public ServiceInsertarFEControlDocumentos(ConfService confService, 
			String numDocumento, 
			String ptoEstablecimiento, 
			String ptoEmision, 
			String numAutorizaSri, 
			String fechaAutoriza, 
			String fechaDocumento, 
			String estado, 
			String usuario, 
			String clave, 
			String tipo){
		try {
			this.confService = confService;
			
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formato1 = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssz");
			TimeZone zone = TimeZone.getTimeZone("UTC");
			format.setTimeZone(zone);
			
			this.numDocumento = numDocumento; 
			this.ptoEstablecimiento = ptoEstablecimiento; 
			this.ptoEmision = ptoEmision; 
			this.numAutorizaSri = numAutorizaSri;  
			this.fechaAutoriza = fechaAutoriza; 
			
			if(fechaDocumento.indexOf("/") == -1){
				this.fechaDocumento = format.format(formato1.parse(fechaDocumento)).replaceAll( "UTC", "-05:00" );
			}else{
				this.fechaDocumento = format.format(formato.parse(fechaDocumento)).replaceAll( "UTC", "-05:00" );
			}
			
			this.estado = estado;
			this.usuario = usuario; 
			this.clave = clave;
			this.tipo = tipo;
			this.ultimoCambio = format.format(new Date()).replaceAll( "UTC", "-05:00" );
		} catch (ParseException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}
	
	public ServRespuestaControl callRespuesta(){
		Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("num_documento", this.numDocumento);
    	auxParametros.put("pto_establecimiento", this.ptoEstablecimiento);
    	auxParametros.put("pto_emision", this.ptoEmision);
    	auxParametros.put("num_autoriza_sri", this.numAutorizaSri);
    	auxParametros.put("fecha_autoriza", this.fechaAutoriza);
    	auxParametros.put("fecha_documento", this.fechaDocumento);
    	auxParametros.put("estado", this.estado);
    	auxParametros.put("usuario", this.usuario);
    	auxParametros.put("clave", this.clave);
    	auxParametros.put("tipo", this.tipo);
    	auxParametros.put("ultimo_cambio", this.ultimoCambio);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getInsertarFEControlDocumentos());
		
		ServRespuestaControl respuestaControl = new ServRespuestaControl(swendysCall.Call());
		
		return respuestaControl;
	}
}
