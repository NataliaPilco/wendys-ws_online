package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDocumentoSucNumFNumDocProve;
import atrums.modelo.Servicio.ServDocumentosSucNumFNumDocProve;

public class ServiceRetencionPorFecha {
	static final Logger log = Logger.getLogger(ServiceRetencionPorFecha.class);
	private final ConfService confService;
	private String fecha;
    
    public ServiceRetencionPorFecha(ConfService confService, String fecha){
        this.confService = confService;
        this.fecha = fecha;
    }
    
    public List<ServDocumentoSucNumFNumDocProve> callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarResumenRetencionPorFecha());
        
        ServDocumentosSucNumFNumDocProve documentosSucNumFNumDocProve = new ServDocumentosSucNumFNumDocProve(swendysCall.Call());
        
        return documentosSucNumFNumDocProve.getDocumentos();
    }
}
