package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDetalleRentecion;
import atrums.modelo.Servicio.ServDetallesRetencion;

public class ServiceRetencionDetallesPorFecha {
	static final Logger log = Logger.getLogger(ServiceRetencionDetallesPorFecha.class);
    private final ConfService confService;
    private String sucursal;
    private String nroDocumento;
    private String fecha;
    
    public ServiceRetencionDetallesPorFecha(ConfService confService, String sucursal, String nroDocumento, String fecha){
        this.confService = confService;
        this.sucursal = sucursal;
        this.nroDocumento = nroDocumento;
        this.fecha = fecha;
    }
	
    public List<ServDetalleRentecion> callDocumentos(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("fecha", this.fecha);
    	
        ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
                auxParametros, 
                confService.getConsultarDetalleRetenciones());
        
        ServDetallesRetencion detallesRetencion = new ServDetallesRetencion(swendysCall.Call(), this.sucursal, this.nroDocumento);
        
        return detallesRetencion.getLineas();
    }

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
