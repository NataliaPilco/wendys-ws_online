package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServFechas;

public class ServiceClaveFecha {
	static final Logger log = Logger.getLogger(ServiceClaveFecha.class);
    private final ConfService confService;
    
    public ServiceClaveFecha(ConfService confService){
        this.confService = confService;
    }
    
    public List<String> callFechas(){
        ServicioWendysCall wendysCall = new ServicioWendysCall(confService.getwSDL(), 
                new Hashtable<String, String>(), 
                confService.getConsultarFechasClave());
        
        ServFechas fechas = new ServFechas(wendysCall.Call());
        
        wendysCall = null;
        return fechas.getFechas();
    }
}
