package atrums.persistencia;

import java.sql.Connection;

import javax.sql.DataSource;

public interface OperacionesBDD {
	public boolean guardarDocumento(Connection connection);
	public boolean actualizarEstado(Connection connection);
	public boolean getBDDDocumentoBaseProcesar(String tipoDocumento, Connection connection);
	public boolean getBDDDocumentoBaseMigrar(String tipoDocumento, Connection connection);
	public Connection getConneccion(DataSource dataSource);
	public boolean getEmpresaPrincipal(Connection connection);
	public boolean existeDato(String sql, Connection connection);
	
	public boolean existeDocumentoImporFCNC(Connection connection);
	public boolean getBDDOadClient(Connection connection);
	public boolean getBDDOadOrg(Connection connection);
	public boolean getBDDOcDoctype(Connection connection);
	public boolean getBDDOcbPartner(Connection connection);
	public boolean getBDDOcbPartnerTrans(Connection connection);
	public boolean getBDDOadUser(Connection connection);
	public boolean getBDDOcoTipoRetencion(Connection connection);
	public boolean getBDDOcoBpRetencionCompra(Connection connection);
	public boolean getBDDOcPocConfiguration(Connection connection);
	
	public boolean saveBDDOcbPartner(Connection connection);
	public boolean saveBDDOcbPartnerTrans(Connection connection);
	public boolean saveBDDOcbGroup(Connection connection);
	public boolean saveBDDOadUser(Connection connection);
	public boolean saveBDDOcLocation(Connection connection);
	public boolean saveBDDOcLocationDes(Connection connection);
	public boolean saveBDDOcLocationPar(Connection connection);
	public boolean saveBDDOcbPartnerLocation(Connection connection);
	public boolean saveBDDOcInvoice(Connection connection);
	public boolean saveBDDOmInout(Connection connection);
	public boolean saveBDDOcoRetencionCompra(Connection connection);
	public boolean saveBDDOcPaymentterm(Connection connection);
	public boolean saveBDDOmPricelist(Connection connection);
	public boolean saveBDDmProductCategory(Connection connection);
	public boolean saveBDDOcTaxcategory(Connection connection);
	public boolean saveBDDOcTax(Connection connection);
	public boolean saveBDDOmProduct(Connection connection);
	public boolean saveBDDOcInvoiceLine(Connection connection);
	public boolean saveBDDOmInoutLine(Connection connection);
	public boolean saveBDDOadUserPortal(Connection connection);
	public boolean saveBDDOadRole(Connection connection);
	public boolean saveBDDOadRoleOrgaccess(Connection connection);
	public boolean saveBDDOadWindowAccess(Connection connection);
	public boolean saveBDDOadUserRoles(Connection connection);
	public boolean saveBDDOcoTipoRetencion(Connection connection);
	public boolean saveBDDOcoBpRetencionCompra(Connection connection);
	public boolean saveBDDOcoRetencionCompraLinea(Connection connection);
	public boolean saveBDDNovedad(Connection connection, String mensaje);
	public boolean updateBDDOmProduct(Connection connection);
	public boolean saveLogServicio(Connection connection, String Mensaje);
	
	public boolean procesarFactura(String total, Connection connection);
	public String buscarPaymentSchedule(Connection connection);
	public boolean iniciarPago(String idpaymentSchedule, Connection connection);
	public boolean savePago(String idpayment, String metodoPago, String amount, Connection connection);
	public boolean savePagolinea(String idpayment, String idpaymentline, String amount, Connection connection);
	public boolean savePaymentScheduledetail(String idpaymentSchedule, String idpaymentline, String amount, Connection connection);
	public boolean procesarPago(String idpayment, boolean dato, Connection connection);
	public boolean terminarFactura(String ispaid, String amountTotal, Connection connection);
	public boolean terminarRetencion(Connection connection);
}
