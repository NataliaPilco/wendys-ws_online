package atrums.persistencia;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDocumentoNroNroEsNroEm;
import atrums.modelo.Servicio.ServDocumentosNroNroEsNroEm;

public class ServiceGuiaRemisionFechaPagina {
	static final Logger log = Logger.getLogger(ServiceGuiaRemisionFechaPagina.class);
	private final ConfService confService;
	private String fecha;
	private String pagina;
	
	public ServiceGuiaRemisionFechaPagina(ConfService confService, String fecha, String pagina){
        this.confService = confService;
        this.fecha = fecha;
        this.pagina = pagina;
    }
	
	public List<ServDocumentoNroNroEsNroEm> callDocumentos(){
		Hashtable<String, String> auxParametros = new Hashtable<String, String>();
		auxParametros.put("fecha", this.fecha);
    	auxParametros.put("numPagina", this.pagina);
    	
    	ServicioWendysCall swendysCall = new ServicioWendysCall(confService.getwSDL(), 
    			auxParametros, 
    			confService.getConsultarResumenGuiasRemisionPaginadasPorFecha());
    	
    	ServDocumentosNroNroEsNroEm documentosNroNroEsNroEm = new ServDocumentosNroNroEsNroEm(swendysCall.Call(),  this.fecha);
    	
    	return documentosNroNroEsNroEm.getDocumentos();
	}
}
