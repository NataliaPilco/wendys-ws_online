/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atrums.persistencia;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServFechas;

import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;

public class ServiceFacturaFecha {
    static final Logger log = Logger.getLogger(ServiceFacturaFecha.class);
    private final ConfService confService;
    
    public ServiceFacturaFecha(ConfService confService){
        this.confService = confService;
    }
    
    public List<String> callFechas(){
        ServicioWendysCall wendysCall = new ServicioWendysCall(this.confService.getwSDL(), 
                new Hashtable<String, String>(), 
                this.confService.getConsultarFechasFactura());
        
        ServFechas fechas = new ServFechas(wendysCall.Call());
        
        wendysCall = null;
        //List <String> auxFechas = new ArrayList<String>();
        //auxFechas.add("2017-09-07");
        return fechas.getFechas();
        //return auxFechas;
    }
}
