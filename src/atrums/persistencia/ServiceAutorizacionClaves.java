package atrums.persistencia;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.SRI.SRIDocumentoAutorizadoClave;

public class ServiceAutorizacionClaves {
	static final Logger log = Logger.getLogger(ServiceAutorizacionClaves.class);
	private ConfService confService;
	private String claveAcceso;
	
	public ServiceAutorizacionClaves(ConfService confService, String claveAcceso){
		this.confService = confService;
		this.claveAcceso = claveAcceso;
	}

	public SRIDocumentoAutorizadoClave CallAutorizado(){
		ServicioSRICall sriCall = new ServicioSRICall(this.confService, 
				"2", 
				null, 
				this.claveAcceso);
		
		SRIDocumentoAutorizadoClave autorizado = new SRIDocumentoAutorizadoClave(sriCall.AutorizadoCall());
		
		return autorizado;
	}
}
