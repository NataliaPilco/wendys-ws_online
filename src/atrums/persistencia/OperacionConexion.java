package atrums.persistencia;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
//import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.postgresql.ds.PGPoolingDataSource;

import atrums.modelo.ConfBDDOpenbravo;

public class OperacionConexion {
	static final Logger log = Logger.getLogger(OperacionConexion.class);
	private static ConfBDDOpenbravo bddOpenbravo = new ConfBDDOpenbravo();
	private static DataSource dataSource = null;
	
	public static DataSource getDataSource() {
		return OperacionConexion.dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		OperacionConexion.dataSource = dataSource;
	}

	static {
		log.info("Inside Database() static method");
		
		PGPoolingDataSource source = new PGPoolingDataSource();
		
		source.setDatabaseName(OperacionConexion.bddOpenbravo.getbDD());
		source.setApplicationName("Migracion de Datos de Wendys");
		source.setServerName(OperacionConexion.bddOpenbravo.getIpBDD());
		source.setPortNumber(new Integer(OperacionConexion.bddOpenbravo.getPuertoBDD()));
		source.setUser(OperacionConexion.bddOpenbravo.getUsuarioBDD());
		source.setPassword(OperacionConexion.bddOpenbravo.getPasswordBDD());
		source.setMaxConnections(50);
		
		OperacionConexion.dataSource = source;
	}
	
	/*public DataSource crearConexionPostgresOpenbravo(String util){
		BasicDataSource basicDataSource = new BasicDataSource();
		DataSource dataSource = null;
		
		try {
			log.info("Creando Conexión");
			
			basicDataSource.setDriverClassName("org.postgresql.Driver");
			basicDataSource.setUsername(this.bddOpenbravo.getUsuarioBDD());
			basicDataSource.setPassword(this.bddOpenbravo.getPasswordBDD());
			basicDataSource.setUrl("jdbc:postgresql://" + 
					this.bddOpenbravo.getIpBDD() + ":" + 
					this.bddOpenbravo.getPuertoBDD() + "/" + 
					this.bddOpenbravo.getbDD());
			basicDataSource.setMaxActive(20);	//50
			basicDataSource.setMaxIdle(10);		//25
			basicDataSource.setMinIdle(1);
			basicDataSource.setInitialSize(1);
			basicDataSource.setMinEvictableIdleTimeMillis(60000);
			basicDataSource.setTimeBetweenEvictionRunsMillis(60000);
			basicDataSource.setNumTestsPerEvictionRun(3);
			basicDataSource.setMaxWait(500);
			
			basicDataSource.setRemoveAbandoned(true);
			basicDataSource.setRemoveAbandonedTimeout(60);
			basicDataSource.setLogAbandoned(true);
			
			basicDataSource.setTestOnBorrow(true);
			basicDataSource.setTestWhileIdle(true);
			basicDataSource.setTestOnReturn(false);
			
			log.info("Ejecutando Validación");
			
			basicDataSource.setValidationQuery("SELECT 1");
			
			log.info("Terminando Validación");
			
			dataSource = basicDataSource;
		} catch (Exception ex) {log.warn(ex.getMessage());}
		
		return dataSource;
	}*/
}
