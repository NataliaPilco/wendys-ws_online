package atrums.persistencia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;

import atrums.dao.firma.XAdESBESSignature;
import atrums.modelo.ConfService;

import org.apache.commons.codec.binary.Base64;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class OperacionesAuxiliares {
	static final Logger log = Logger.getLogger(OperacionesAuxiliares.class);
	private static Cipher s_cipher = null;
	private static SecretKey s_key = null;
	
	public String normalizacionPalabras(String palabras) {
		String palabraAux = palabras;
	    palabraAux = palabraAux.replaceAll("�", "a");
	    palabraAux = palabraAux.replaceAll("�", "e");
	    palabraAux = palabraAux.replaceAll("�", "i");
	    palabraAux = palabraAux.replaceAll("�", "o");
	    palabraAux = palabraAux.replaceAll("�", "u");
	    return palabraAux;
	}
	
	public String generarclaveacceso(String fecha, 
			String tipoComprobante, 
			String ruc,
			String tipoAmbiente, 
			String serie, 
			String secuencial, 
			String codigoNum,
		    String tipoEmision) {
		String clave = fecha + tipoComprobante + ruc + tipoAmbiente + serie + secuencial + codigoNum + tipoEmision;
		int[] claveAux = new int[48];
		
		for (int i = 0; i < clave.length(); i++) {
			claveAux[47 - i] = Integer.parseInt(clave.substring(i, i + 1));
		}

		int acumulado = 0;
		int j = 2;

		for (int i = 0; i < claveAux.length; i++) {
			acumulado = acumulado + (claveAux[i] * j);
			if (j == 7) {j = 1;}
			j++;
		}
		
		Integer numVer = 11 - (acumulado % 11);
		if (numVer >= 10) {numVer = 11 - numVer;}
		clave = clave + numVer.toString();
		
		return clave;
	}
	
	public File firmarDocumento(File file, ConfService service) {
		File flDocumentoFirmado = null;
		try {
			flDocumentoFirmado = XAdESBESSignature.firmar(file, service.getDireccionFirma(), service.getPasswordFirma());
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		return flDocumentoFirmado;
	}
	
	public byte[] filetobyte(File file) {
		byte[] bytesen = null;
		
		try {
			byte[] bytes = new byte[(int) file.length()];
			FileInputStream is;
			
			is = new FileInputStream(file);
			is.read(bytes);
			is.close();
			
			bytesen = Base64.encodeBase64(bytes);
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
	    return bytesen;
	}
	
	public String stringtostring64Au(String comprobante, 
			String estado,
			String numeroAutorizacion, 
			String fechaAutorizacion, 
			String ambiente) {
		String docXMLFinal = null;
		
		try{
			File flDoc = File.createTempFile("documento", ".xml", null);
			
			Document docXML = DocumentHelper.createDocument();
			OutputFormat ofFormat = OutputFormat.createPrettyPrint();
			ofFormat.setEncoding("UTF-8");
			Element elmact = docXML.addElement("autorizacion");
			elmact.addElement("estado").addText(estado);
			elmact.addElement("numeroAutorizacion").addText(numeroAutorizacion);
			elmact.addElement("fechaAutorizacion").addText(fechaAutorizacion);
			elmact.addElement("ambiente").addText(ambiente);

			SAXReader reader = new SAXReader();
			Document docAux = reader.read(new StringReader(comprobante));
			Element elmcom = elmact.addElement("comprobante");
			elmcom.add(docAux.getRootElement());

			XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flDoc),"UTF-8"), ofFormat);
			writer.write(docXML);
			writer.flush();
			writer.close();

			byte[] bytes = new byte[(int) flDoc.length()];

			FileInputStream is = new FileInputStream(flDoc);

			is.read(bytes);
			is.close();
			
			byte[] bytesen = Base64.encodeBase64(bytes);
			docXMLFinal = new String(bytesen, "UTF-8");
			flDoc.delete();
		} catch (DocumentException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (UnsupportedEncodingException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (FileNotFoundException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
		
		return docXMLFinal;
	}
	
	public File stringtoDocumento(String comprobante, 
			String estado,
			String numeroAutorizacion, 
			String fechaAutorizacion, 
			String ambiente) {
		File flDoc = null;
		
		try{
			flDoc = File.createTempFile("documento", ".xml", null);
			
			Document docXML = DocumentHelper.createDocument();
			OutputFormat ofFormat = OutputFormat.createPrettyPrint();
			ofFormat.setEncoding("UTF-8");
			Element elmact = docXML.addElement("autorizacion");
			elmact.addElement("estado").addText(estado);
			elmact.addElement("numeroAutorizacion").addText(numeroAutorizacion);
			elmact.addElement("fechaAutorizacion").addText(fechaAutorizacion);
			elmact.addElement("ambiente").addText(ambiente);

			SAXReader reader = new SAXReader();
			Document docAux = reader.read(new StringReader(comprobante));
			Element elmcom = elmact.addElement("comprobante");
			elmcom.add(docAux.getRootElement());

			XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flDoc),"UTF-8"), ofFormat);
			writer.write(docXML);
			writer.flush();
			writer.close();
			
		} catch (DocumentException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			flDoc.deleteOnExit();
			flDoc = null;
		} catch (UnsupportedEncodingException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			flDoc.deleteOnExit();
			flDoc = null;
		} catch (FileNotFoundException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			flDoc.deleteOnExit();
			flDoc = null;
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
			flDoc.deleteOnExit();
			flDoc = null;
		}
		
		return flDoc;
	}
	
	public static void printSoapRes(SOAPMessage soapMes) throws SOAPException, TransformerException {
        TransformerFactory transformerFac = TransformerFactory.newInstance();
        Transformer transfor = transformerFac.newTransformer();
        Source sourceCon = soapMes.getSOAPPart().getContent();
        System.out.println("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transfor.transform(sourceCon, result);
	}
	
	public String encryptSh1(String x) throws Exception {
		MessageDigest d = null;
        d = MessageDigest.getInstance("SHA-1");
        d.reset();
        d.update(x.getBytes());
        
        byte[] bytesen = Base64.encodeBase64(d.digest());
        return new String(bytesen, "UTF-8");
	}
	
	public String dencryptSh1(String x) {
		return decrypt(x);
	}
	
	private String decrypt(String value){
		if (value == null) return null;
	    if (value.length() == 0) return value;
	    if (s_cipher == null) initCipher();
	    
	    byte out[] = null;
	    byte decode[];
	    
	    try {
	    	decode = org.apache.commons.codec.binary.Base64.decodeBase64(value.getBytes("UTF-8"));
	    	s_cipher.init(Cipher.DECRYPT_MODE, s_key, s_cipher.getParameters());
	    	out = s_cipher.doFinal(decode);
	    } catch (Exception ex) {
	    	log.error(ex.getMessage());
	    }
	    
	    return new String(out);
	}
	
	private static void initCipher() {
	    try {
	    	s_key = new SecretKeySpec(new byte[] { 100, 25, 28, -122, -26, 94, -3, -72 }, "DES");
	    	s_cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
	    } catch (Exception ex) {
	    	log.error(ex.getMessage());
	    }
	}
	
	public File generarPDF(String baseDesin, 
			String reporte, 
			String idDocumento, 
			String claveAcceso, 
			Connection connection){
		File flTemp = null;

	    try {
	    	Map<String, String> parameters = new HashMap<String, String>();
	        parameters.put("DOCUMENT_ID", idDocumento);
	        parameters.put("BASE_DESIGN", baseDesin);

	        JasperReport jasperRepo = JasperCompileManager.compileReport(baseDesin + reporte);

	        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, connection);

	        
	        String nombreArch = "documento" + "-" + claveAcceso;
	        OutputStream out = null;

	        flTemp = File.createTempFile(nombreArch, ".pdf", null);

	        out = new FileOutputStream(flTemp);
	        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
	        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	        out.write(byteout.toByteArray());
	        out.flush();
	        out.close();
	    } catch (JRException ex) {
	      // TODO Auto-generated catch block
	      log.error(ex.getMessage());
	    } catch (IOException ex) {
	      // TODO Auto-generated catch block
	    	log.error(ex.getMessage());
	    } catch (Exception ex) {
	      // TODO: handle exception
	    	log.error(ex.getMessage());
	    }
	    
	    return flTemp;
	}
	
	public String limpiarCodigoProducto(String codigo){
		String auxCodigo = codigo;
		String puntuacion = "�!�?.,;:'&, ";
	    int iterador = 0;
	    String caracterInspeccionado;
	    
	    while (iterador < auxCodigo.length()) {
	        caracterInspeccionado = auxCodigo.substring(iterador, iterador+1);
	        if (puntuacion.contains(caracterInspeccionado)) {
	            auxCodigo = auxCodigo.replaceAll("\\"+caracterInspeccionado, "");
	        } else {
	            iterador++;
	        }
	    }
	    
	    return auxCodigo.toLowerCase();
	}
	
	public boolean isNumeric(String cadena){
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (Exception ex){
			return false;
		}
	}
}
