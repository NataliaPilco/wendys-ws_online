package atrums.persistencia;

import java.util.Hashtable;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.modelo.Servicio.ServDireccion;

public class ServiceDireccion {
	static final Logger log = Logger.getLogger(ServiceDireccion.class);
    private final ConfService confService;
    private String identificacion;
    
    public ServiceDireccion(ConfService confService, String identificacion){
        this.confService = confService;
        this.identificacion = identificacion;
    }
    
    public ServDireccion callDireccion(){
    	Hashtable<String, String> auxParametros = new Hashtable<String, String>();
    	auxParametros.put("identificacion", this.identificacion);
    	
        ServicioWendysCall wendysCall = new ServicioWendysCall(confService.getwSDL(), 
        		auxParametros, 
                confService.getConsultarDireccionClientePorIdentificacion());
        
        ServDireccion direccion = new ServDireccion(wendysCall.Call());
        
        return direccion;
    }
}
