package atrums.modelo.Servicio;

import org.apache.log4j.Logger;

public class ServDocumentoNroNroEsNroEm {
	static final Logger log = Logger.getLogger(ServDocumentoNroNroEsNroEm.class);
	private String nroDocumento;
	private String nroEstablecimiento;
	private String nroEmision;
	private String tipoDoc;
	private String fechadocumento;
    
    public ServDocumentoNroNroEsNroEm(String nroDocumento, 
    		String nroEstablecimiento, 
    		String nroEmision, 
    		String tipoDoc, 
    		String fechadocumento){
        this.nroDocumento = nroDocumento;
        this.nroEstablecimiento = nroEstablecimiento;
        this.nroEmision = nroEmision;
        this.tipoDoc = tipoDoc;
        this.fechadocumento = fechadocumento;
    }

	public String getFechadocumento() {
		return fechadocumento;
	}

	public void setFechadocumento(String fechadocumento) {
		this.fechadocumento = fechadocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getNroEstablecimiento() {
		return nroEstablecimiento;
	}

	public void setNroEstablecimiento(String nroEstablecimiento) {
		this.nroEstablecimiento = nroEstablecimiento;
	}

	public String getNroEmision() {
		return nroEmision;
	}

	public void setNroEmision(String nroEmision) {
		this.nroEmision = nroEmision;
	}
	
	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}
}
