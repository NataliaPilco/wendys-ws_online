package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.NodeList;

public class ServDetallesRetencion {
	static final Logger log = Logger.getLogger(ServDetallesRetencion.class);
	private List<ServDetalleRentecion> lineas;
	
	public ServDetallesRetencion() {
		super();
	}

	public ServDetallesRetencion(SOAPMessage soapMes, String sucursal, String nroDocumento){
		lineas = new ArrayList<ServDetalleRentecion>();
		try{
			if(soapMes != null){
				int tamanio = soapMes.getSOAPBody().getElementsByTagName("NO_RETENCION").getLength();
				if(tamanio > 0){
					NodeList datosNroRetencion = soapMes.getSOAPBody().getElementsByTagName("NO_RETENCION");
					NodeList datosSucursal = soapMes.getSOAPBody().getElementsByTagName("SUCURSAL");
					NodeList datostipo = soapMes.getSOAPBody().getElementsByTagName("TIPO");
					NodeList datosBaseImpRetencion = soapMes.getSOAPBody().getElementsByTagName("BASE_IMP_RETENCION");
					NodeList datosCodigoRetencion = soapMes.getSOAPBody().getElementsByTagName("CODIGO_RETENCION");
					NodeList datosValorRetencion = soapMes.getSOAPBody().getElementsByTagName("VALOR_RETENCION");
					
					for(int i=0;i<tamanio;i++){
						if(datosNroRetencion.item(i).getFirstChild().getNodeValue().equals(nroDocumento) && 
								datosSucursal.item(i).getFirstChild().getNodeValue().equals(sucursal)){
							ServDetalleRentecion auxDoc = new ServDetalleRentecion(
									datosSucursal.item(i).getFirstChild().getNodeValue(), 
									datosNroRetencion.item(i).getFirstChild().getNodeValue(), 
									datostipo.item(i).getFirstChild().getNodeValue(), 
									datosBaseImpRetencion.item(i).getFirstChild().getNodeValue(), 
									datosCodigoRetencion.item(i).getFirstChild().getNodeValue(), 
									datosValorRetencion.item(i).getFirstChild().getNodeValue());
							
							lineas.add(auxDoc);
						}
					}
				}
			}
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	public List<ServDetalleRentecion> getLineas() {
		return lineas;
	}

	public void setLineas(List<ServDetalleRentecion> lineas) {
		this.lineas = lineas;
	}
}
