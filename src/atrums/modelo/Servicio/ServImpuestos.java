package atrums.modelo.Servicio;

import org.apache.log4j.Logger;

public class ServImpuestos {
	static final Logger log = Logger.getLogger(ServImpuestos.class);
	private String codigoImpuesto = null;
	private String porcentajeImpuesto = null;
	private double baseImponible = 0;
	private double valor = 0;
	
	public ServImpuestos() {
		super();
	}

	public String getCodigoImpuesto() {
		return codigoImpuesto;
	}

	public void setCodigoImpuesto(String codigoImpuesto) {
		this.codigoImpuesto = codigoImpuesto;
	}

	public String getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}

	public void setPorcentajeImpuesto(String porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}

	public double getBaseImponible() {
		return baseImponible;
	}

	public void setBaseImponible(double baseImponible) {
		this.baseImponible = baseImponible;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
}
