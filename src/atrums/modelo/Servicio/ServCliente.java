package atrums.modelo.Servicio;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import atrums.modelo.ConfService;
import atrums.persistencia.ServiceDireccion;
import atrums.persistencia.ServiceEmail;

public class ServCliente {
	static final Logger log = Logger.getLogger(ServCliente.class);
	private String identificacion = null;
	private String razonSocial = null;
	private String nombreComercial = null;
	private String tipo = null;
	private ServDireccion direccion = new ServDireccion();
	private ServEmail email = new ServEmail();
	
	public ServCliente() {
		super();
	}

	public ServCliente(SOAPMessage soapMes, ConfService confService){
		try{
			if(soapMes != null){
				int tamanio = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").getLength();
				if(tamanio > 0){
					this.identificacion = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").item(0).getFirstChild().getNodeValue();
                    this.nombreComercial = soapMes.getSOAPBody().getElementsByTagName("NOMBRE_COMERCIAL").item(0).getFirstChild().getNodeValue();
                    this.razonSocial = soapMes.getSOAPBody().getElementsByTagName("RAZON_SOCIAL").item(0).getFirstChild().getNodeValue();
                    
                    if(this.identificacion.equals("9999999999999")){
						this.tipo = "07";
					}else if(this.identificacion.length() == 10){
						this.tipo = "04";
					}else if(this.identificacion.length() == 13 && this.identificacion.substring(10, this.identificacion.length()).equals("001")){
						this.tipo = "05";
					}else{
						this.tipo = "06";
					}
                    
                    ServiceDireccion serviceDireccion = new ServiceDireccion(confService, this.identificacion);
                    this.direccion = serviceDireccion.callDireccion();
                    
                    ServiceEmail serviceEmail = new ServiceEmail(confService, this.identificacion);
                    this.email = serviceEmail.callEmail();
				}
			}
		} catch (SOAPException ex) {
			// TODO Auto-generated catcsh block
			log.error(ex.getMessage());
		}
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public ServEmail getEmail() {
		return email;
	}

	public void setEmail(ServEmail email) {
		this.email = email;
	}

	public ServDireccion getDireccion() {
		return direccion;
	}

	public void setDireccion(ServDireccion direccion) {
		this.direccion = direccion;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
}
