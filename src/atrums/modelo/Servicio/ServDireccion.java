package atrums.modelo.Servicio;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

public class ServDireccion {
	static final Logger log = Logger.getLogger(ServDireccion.class);
	private String identificacion = null;
	private String direccion = null;
	private String direccion2 = null;
	
	public ServDireccion() {
		super();
	}

	public ServDireccion(SOAPMessage soapMes){
		try{
			if(soapMes != null){
				int tamanio = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").getLength();
				if(tamanio > 0){
					this.direccion = "N/A";
					this.direccion2 = "N/A";
					this.identificacion = soapMes.getSOAPBody().getElementsByTagName("ID_TERCERO").item(0).getFirstChild().getNodeValue();
					this.direccion = soapMes.getSOAPBody().getElementsByTagName("DIRECCION1").item(0).getFirstChild().getNodeValue();
					this.direccion2 = soapMes.getSOAPBody().getElementsByTagName("DIRECCION2").item(0).getFirstChild().getNodeValue();
				}
			}
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}
	
	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
