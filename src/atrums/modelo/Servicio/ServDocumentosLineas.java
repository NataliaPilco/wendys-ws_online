package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ServDocumentosLineas {
	static final Logger log = Logger.getLogger(ServDocumentosLineas.class);
	private List<ServDocumentoLinea> lineas;
	
	public ServDocumentosLineas(String fecha, SOAPMessage soapMes){
		lineas = new ArrayList<ServDocumentoLinea>();
        try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO").getLength();
                if(tamanio != 0){
                	NodeList datosNroDoc = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO");
                	NodeList datosTipoDoc= soapMes.getSOAPBody().getElementsByTagName("TIPODOC");
                    NodeList datosFeFac = soapMes.getSOAPBody().getElementsByTagName("FECHA_FACTURA");
                    NodeList datosFeCont = soapMes.getSOAPBody().getElementsByTagName("FECHA_CONTABI");
                    NodeList datosFeGuia = soapMes.getSOAPBody().getElementsByTagName("FECHA");
                    NodeList datosIdCli = soapMes.getSOAPBody().getElementsByTagName("IDENTIFA_CLIENTE");
                    NodeList datosDesti = soapMes.getSOAPBody().getElementsByTagName("RUC_DESTINATARIO");
                    NodeList datosTransp = soapMes.getSOAPBody().getElementsByTagName("RUC_TRASPORTISTA");
                    
                    NodeList datosSubTo = soapMes.getSOAPBody().getElementsByTagName("SUBTOTAL");
                    NodeList datosTotFac = soapMes.getSOAPBody().getElementsByTagName("TOTAL_FAC");
                    NodeList datosNroEs = soapMes.getSOAPBody().getElementsByTagName("NO_ESTABLECIMIENTO");
                    NodeList datosNroEsGuia = soapMes.getSOAPBody().getElementsByTagName("PTO_ESTABLECIMIENTO");
                    NodeList datosNroEm = soapMes.getSOAPBody().getElementsByTagName("PTO_EMISION");
                    NodeList datosProd = soapMes.getSOAPBody().getElementsByTagName("PRODUCTO");
                    NodeList datosCant = soapMes.getSOAPBody().getElementsByTagName("CANTIDAD");
                    NodeList datosPrec = soapMes.getSOAPBody().getElementsByTagName("PRECIO");
                    NodeList datosTotLi = soapMes.getSOAPBody().getElementsByTagName("TOTAL_LINEA");
                    NodeList datosImpu = soapMes.getSOAPBody().getElementsByTagName("IMPUESTO");
                    NodeList datosValImpu = soapMes.getSOAPBody().getElementsByTagName("VALOR_IMPUESTO");
                    NodeList datosBaseImpo = soapMes.getSOAPBody().getElementsByTagName("BASE_IMPONIBLE_LINEA");
                    NodeList datosNomCom = soapMes.getSOAPBody().getElementsByTagName("NOMBRE_COMERCIAL");
                    NodeList datosRazSoc = soapMes.getSOAPBody().getElementsByTagName("RAZON_SOCIAL");
                    NodeList datosRazSocDes = soapMes.getSOAPBody().getElementsByTagName("RAZON_DESTINATARIO");
                    NodeList datosRazSocTransp = soapMes.getSOAPBody().getElementsByTagName("RAZON_SOCIAL_TRANSPORTISTA");
                    
                    NodeList datosDescuento = soapMes.getSOAPBody().getElementsByTagName("DESCUENTO");
                    
                    NodeList datometodopago = soapMes.getSOAPBody().getElementsByTagName("METODO_PAGO");
                    
                    NodeList datosSucursal = soapMes.getSOAPBody().getElementsByTagName("SUCURSAL");
                    NodeList datosNroFact = soapMes.getSOAPBody().getElementsByTagName("NUM_FACTURA");
                    NodeList datosProvee = soapMes.getSOAPBody().getElementsByTagName("PROVEEDOR");
                    NodeList datosRucProve = soapMes.getSOAPBody().getElementsByTagName("RUC_PROVEEDOR");
                    NodeList datosDirProve = soapMes.getSOAPBody().getElementsByTagName("DIRECCION_PROVEEDOR");
                    NodeList datosFechaEmision = soapMes.getSOAPBody().getElementsByTagName("FECHA_EMISION_RETEN");
                    NodeList datosNroComproVenta = soapMes.getSOAPBody().getElementsByTagName("NUM_COMPROBANTE_VENTA");
                    NodeList datosTotalReten = soapMes.getSOAPBody().getElementsByTagName("TOTAL_RETENCION");
                    NodeList datosTipoDocumento = soapMes.getSOAPBody().getElementsByTagName("TIPO_DOCUMENTO");
                    NodeList datosFechaConta = soapMes.getSOAPBody().getElementsByTagName("FECHA_CONTABLE");
                    
                    NodeList numDocRelacionado = soapMes.getSOAPBody().getElementsByTagName("NUM_DOC_RELACIONA");
                    NodeList fechaNumDocRelacionado = soapMes.getSOAPBody().getElementsByTagName("FECHA_NUM_DOC_RELACIONA");
                    
                    NodeList dirPartida = soapMes.getSOAPBody().getElementsByTagName("DIRECCION_PARTIDA");
                    NodeList fechaIniTransp = soapMes.getSOAPBody().getElementsByTagName("FECHA_INICIO_TRANSPORTE");
                    NodeList fechaFinTransp = soapMes.getSOAPBody().getElementsByTagName("FECHA_FIN_TRANSPORTE");
                    NodeList placa = soapMes.getSOAPBody().getElementsByTagName("PLACA");
                    NodeList dirDestinatario = soapMes.getSOAPBody().getElementsByTagName("DIRECCION_DESTINATARIO");
                    NodeList motivoTras = soapMes.getSOAPBody().getElementsByTagName("MOTIVO_TRASLADO");
                    
                    NodeList datosProdGuia = soapMes.getSOAPBody().getElementsByTagName("CODIGO_PRODUCTO");
                    NodeList datosDescripcion = soapMes.getSOAPBody().getElementsByTagName("DESCRIPCION");
                    
                    
                    NodeList formapago1 = soapMes.getSOAPBody().getElementsByTagName("FORMA_PAGO1");
                    NodeList formapago2 = soapMes.getSOAPBody().getElementsByTagName("FORMA_PAGO2");
                    NodeList formapago3 = soapMes.getSOAPBody().getElementsByTagName("FORMA_PAGO3");
                    
                    for(int i=0; i<tamanio; i++){
                    	ServDocumentoLinea auxDoc = new ServDocumentoLinea();
                    	
                    	List<ServPago> auxPagos = new ArrayList<ServPago>();
                    	
                    	if(formapago1.getLength() > 0){
                    		Node nodpaux = formapago1.item(i);
                    		if(nodpaux != null){
                    			Node nodpauxaux = nodpaux.getFirstChild();
                    			if(nodpauxaux != null){
                    				String auxMetodo = formapago1.item(i).getFirstChild().getNodeValue();
                    				if(!auxMetodo.equals("00") && auxMetodo.indexOf("-") != -1){
                    					ServPago auxPago = new ServPago();
                    					auxPago.setMetodo(auxMetodo.substring(0,auxMetodo.indexOf("-")).trim());
                    					auxPago.setMonto(auxMetodo.substring(auxMetodo.indexOf("-") + 1,auxMetodo.length()).trim());
                    					auxPagos.add(auxPago);
                    				}
                    			}
                    		}
                    	}
                    	
                    	if(formapago2.getLength() > 0){
                    		Node nodpaux = formapago2.item(i);
                    		if(nodpaux != null){
                    			Node nodpauxaux = nodpaux.getFirstChild();
                    			if(nodpauxaux != null){
                    				String auxMetodo = formapago2.item(i).getFirstChild().getNodeValue();
                    				if(!auxMetodo.equals("00") && auxMetodo.indexOf("-") != -1){
                    					ServPago auxPago = new ServPago();
                    					auxPago.setMetodo(auxMetodo.substring(0,auxMetodo.indexOf("-")).trim());
                    					auxPago.setMonto(auxMetodo.substring(auxMetodo.indexOf("-") + 1,auxMetodo.length()).trim());
                    					auxPagos.add(auxPago);
                    				}
                    			}
                    		}
                    	}
                    	
                    	if(formapago3.getLength() > 0){
                    		Node nodpaux = formapago3.item(i);
                    		if(nodpaux != null){
                    			Node nodpauxaux = nodpaux.getFirstChild();
                    			if(nodpauxaux != null){
                    				String auxMetodo = formapago3.item(i).getFirstChild().getNodeValue();
                    				if(!auxMetodo.equals("00") && auxMetodo.indexOf("-") != -1){
                    					ServPago auxPago = new ServPago();
                    					auxPago.setMetodo(auxMetodo.substring(0,auxMetodo.indexOf("-")).trim());
                    					auxPago.setMonto(auxMetodo.substring(auxMetodo.indexOf("-") + 1,auxMetodo.length()).trim());
                    					auxPagos.add(auxPago);
                    				}
                    			}
                    		}
                    	}
                    	
                    	auxDoc.setPagos(auxPagos);
                    	
                    	if(datosIdCli.getLength() > 0){
                    		String idCli = null;
                    		
                    		Node nodaux = datosIdCli.item(i);
                    		if(nodaux != null){
                    			Node nodauxaux = nodaux.getFirstChild();
                    			if(nodauxaux != null){
                    				idCli = datosIdCli.item(i).getFirstChild().getNodeValue();
                    			}
                    		}
                    		
                    		if(idCli == null){
                    			auxDoc.setCliente(idCli);
            					
                    			if(datosRazSoc.getLength() > 0){
            						auxDoc.setRazonSocial(datosRazSoc.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocial("N/A");
            					}
            					
            					if(datosNomCom.getLength() > 0){
            						auxDoc.setNombreComercial(datosNomCom.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setNombreComercial("N/A");
            					}
                    		}else if(idCli.equals("9999999999") || 
                    				idCli.equals("9999999999999")) {
                    			auxDoc.setCliente("9999999999999");
            					auxDoc.setRazonSocial("CONSUMIDOR FINAL");
            					auxDoc.setNombreComercial("CONSUMIDOR FINAL");
            				}else{
            					auxDoc.setCliente(idCli);
            					
            					if(datosRazSoc.getLength() > 0){
            						auxDoc.setRazonSocial(datosRazSoc.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocial("N/A");
            					}
            					
            					if(datosNomCom.getLength() > 0){
            						auxDoc.setNombreComercial(datosNomCom.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setNombreComercial("N/A");
            					}
            				}
            			}
                    	
                    	if(datosDesti.getLength() > 0){
                    		String idDes = null;
                    		
                    		Node nodaux = datosDesti.item(i);
                    		if(nodaux != null){
                    			Node nodauxaux = nodaux.getFirstChild();
                    			if(nodauxaux != null){
                    				idDes = datosDesti.item(i).getFirstChild().getNodeValue();
                    			}
                    		}
                    		
                    		if(idDes == null){
                    			auxDoc.setDestinatario(idDes);
            					
                    			if(datosRazSocDes.getLength() > 0){
            						auxDoc.setRazonSocial(datosRazSocDes.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocial("N/A");
            					}
                    		}else if(idDes.equals("9999999999") || 
                    				idDes.equals("9999999999999")) {
                    			auxDoc.setCliente("9999999999999");
            					auxDoc.setRazonSocial("CONSUMIDOR FINAL");
            					auxDoc.setNombreComercial("CONSUMIDOR FINAL");
            				}else{
            					auxDoc.setDestinatario(idDes);
            					
            					if(datosRazSocDes.getLength() > 0){
            						auxDoc.setRazonSocial(datosRazSocDes.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocial("N/A");
            					}
            				}
            			}
                    	
                    	if(datosTransp.getLength() > 0){
                    		String idTransp = null;
                    		
                    		Node nodaux = datosTransp.item(i);
                    		if(nodaux != null){
                    			Node nodauxaux = nodaux.getFirstChild();
                    			if(nodauxaux != null){
                    				idTransp = datosTransp.item(i).getFirstChild().getNodeValue();
                    			}
                    		}
                    		
                    		if(idTransp == null){
                    			auxDoc.setTransportita(idTransp);
            					
                    			if(datosRazSocTransp.getLength() > 0){
            						auxDoc.setRazonSocial(datosRazSocDes.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocial("N/A");
            					}
                    		}else if(idTransp.equals("9999999999") || 
                    				idTransp.equals("9999999999999")) {
                    			auxDoc.setTransportita("9999999999999");
            					auxDoc.setRazonSocialTransp("CONSUMIDOR FINAL");
            					auxDoc.setNombreComercialTransp("CONSUMIDOR FINAL");
            				}else{
            					auxDoc.setTransportita(idTransp);
            					
            					if(datosRazSocTransp.getLength() > 0){
            						auxDoc.setRazonSocialTransp(datosRazSocTransp.item(i).getFirstChild().getNodeValue());
            					}else{
            						auxDoc.setRazonSocialTransp("N/A");
            					}
            				}
            			}
                    	
                    	if(datosNroDoc.getLength() > 0){
                    		auxDoc.setNroDocumento(datosNroDoc.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosTipoDoc.getLength() > 0){
                    		auxDoc.setTipoDoc(datosTipoDoc.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosFeFac.getLength() > 0){
                    		auxDoc.setFechaFactura(datosFeFac.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosFeCont.getLength() > 0){
                    		auxDoc.setFechaContable(datosFeCont.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosFeGuia.getLength() > 0){
                    		auxDoc.setFechaFactura(datosFeGuia.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosSubTo.getLength() > 0){
                    		auxDoc.setSubTotal(datosSubTo.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosTotFac.getLength() > 0){
                    		auxDoc.setTotalFac(datosTotFac.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosNroEs.getLength() > 0){
                    		auxDoc.setNroEstablecimiento(datosNroEs.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosNroEsGuia.getLength() > 0){
                    		auxDoc.setNroEstablecimiento(datosNroEsGuia.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosNroEm.getLength() > 0){
                    		auxDoc.setNroEmision(datosNroEm.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datometodopago.getLength() > 0){
                    		String metodoPago = null;
                    		
                    		Node nodauxPago = datometodopago.item(i);
                    		if(nodauxPago != null){
                    			Node nodauxauxPago = nodauxPago.getFirstChild();
                    			if(nodauxauxPago != null){
                    				metodoPago = datometodopago.item(i).getFirstChild().getNodeValue();
                    			}
                    		}
                    		
                    		auxDoc.setMetodoPago(metodoPago.toUpperCase());
                    	}
                    	
                    	if(datosProd.getLength() > 0){
                    		String idProducto = null;
                    		
                    		Node nodauxPro = datosProd.item(i);
                    		if(nodauxPro != null){
                    			Node nodauxauxPro = nodauxPro.getFirstChild();
                    			if(nodauxauxPro != null){
                    				idProducto = datosProd.item(i).getFirstChild().getNodeValue();
                    			}else{
                    				auxDoc.setMensajeError("No hay un producto en las l�neas del documento que el web service provee");
                    			}
                    		}
                    		auxDoc.setProducto(idProducto);
                    	}
                    	
                    	if(datosProdGuia.getLength() > 0){
                    		String idProducto = null;
                    		
                    		Node nodauxPro = datosProdGuia.item(i);
                    		if(nodauxPro != null){
                    			Node nodauxauxPro = nodauxPro.getFirstChild();
                    			if(nodauxauxPro != null){
                    				idProducto = datosProdGuia.item(i).getFirstChild().getNodeValue();
                    			}else{
                    				auxDoc.setMensajeError("No hay un producto en las l�neas del documento que el web service provee");
                    			}
                    		}
                    		auxDoc.setProducto(idProducto);
                    	}
                    	
                    	if(datosDescripcion.getLength() > 0){
                    		String descripcion = null;
                    		
                    		Node nodauxProDes = datosDescripcion.item(i);
                    		if(nodauxProDes != null){
                    			Node nodauxauxProDes = nodauxProDes.getFirstChild();
                    			if(nodauxauxProDes != null){
                    				descripcion = datosDescripcion.item(i).getFirstChild().getNodeValue();
                    			}else{
                    				auxDoc.setMensajeError("No hay la descripci�n en las l�neas del documento que el web service provee");
                    			}
                    		}
                    		auxDoc.setDescripcion(descripcion);
                    	}
                    	
                    	if(datosCant.getLength() > 0){
                    		auxDoc.setCantidad(datosCant.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosPrec.getLength() > 0){
                    		auxDoc.setPrecio(datosPrec.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosTotLi.getLength() > 0){
                    		auxDoc.setTotalLinea(datosTotLi.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosDescuento.getLength() > 0){
                    		auxDoc.setDescuento(datosDescuento.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosImpu.getLength() > 0){
                    		auxDoc.setImpuesto(datosImpu.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosValImpu.getLength() > 0){
                    		auxDoc.setValorImpuesto(datosValImpu.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosBaseImpo.getLength() > 0){
                    		auxDoc.setBaseImponible(datosBaseImpo.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosSucursal.getLength() > 0){
                    		auxDoc.setSucursal(datosSucursal.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosNroFact.getLength() > 0){
                    		auxDoc.setNroFactura(datosNroFact.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosProvee.getLength() > 0){
                    		auxDoc.setProveedor(datosProvee.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosRucProve.getLength() > 0){
                    		auxDoc.setRucProveedor(datosRucProve.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosDirProve.getLength() > 0){
                    		auxDoc.setDirProveedor(datosDirProve.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosFechaEmision.getLength() > 0){
                    		auxDoc.setFechaEmision(datosFechaEmision.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosNroComproVenta.getLength() > 0){
                    		auxDoc.setNroComprobanteVenta(datosNroComproVenta.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosTotalReten.getLength() > 0){
                    		auxDoc.setTotalRetencion(datosTotalReten.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(datosTipoDocumento.getLength() > 0){
                    		auxDoc.setTipoDocumento(datosTipoDocumento.item(i).getFirstChild().getNodeValue());
                    	}

                    	if(datosFechaConta.getLength() > 0){
                    		auxDoc.setFechaContable(datosFechaConta.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(numDocRelacionado.getLength() > 0){
                    		auxDoc.setNumDocRelacionado(numDocRelacionado.item(i).getFirstChild().getNodeValue());
                    		
                    		if(auxDoc.getNumDocRelacionado().length() < 17){
                                String numest = auxDoc.getNumDocRelacionado().substring(0, 3);
                                String numemi = auxDoc.getNumDocRelacionado().substring(auxDoc.getNumDocRelacionado().indexOf("-") + 1, auxDoc.getNumDocRelacionado().indexOf("-") + 4);
                                String numdoc = auxDoc.getNumDocRelacionado().substring(auxDoc.getNumDocRelacionado().indexOf("-", auxDoc.getNumDocRelacionado().indexOf("-") + 1) + 1, auxDoc.getNumDocRelacionado().length());
                                String secuencial = numdoc;
                                
                                for (int j = 0; j < (9 - numdoc.length()); j++) {
                                	secuencial = "0" + secuencial;                             
                                }
                                
                                auxDoc.setNumDocRelacionado(numest + "-" + numemi + "-" + secuencial);
                            }
                    	}
                    	
                    	if(fechaNumDocRelacionado.getLength() > 0){
                    		auxDoc.setFechaNumDocRelaciona(fechaNumDocRelacionado.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(dirPartida.getLength() > 0){
                    		auxDoc.setDirPartida(dirPartida.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(fechaIniTransp.getLength() > 0){
                    		auxDoc.setFechaIniTransp(fechaIniTransp.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(fechaFinTransp.getLength() > 0){
                    		auxDoc.setFechaFinTransp(fechaFinTransp.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(placa.getLength() > 0){
                    		auxDoc.setPlaca(placa.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(dirDestinatario.getLength() > 0){
                    		auxDoc.setDirDestinatario(dirDestinatario.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(motivoTras.getLength() > 0){
                    		auxDoc.setMotivoTras(motivoTras.item(i).getFirstChild().getNodeValue());
                    	}
                    	
                    	if(fecha.equals(auxDoc.getFechaFactura()) || fecha.equals(auxDoc.getFechaFactura())){
                    		lineas.add(auxDoc);
                    	}
                    }
                }
            }
        } catch(DOMException ex){
            log.error(ex.getMessage());
        } catch(SOAPException ex){
        	log.error(ex.getMessage());
        } catch (Exception ex) {
        	log.error(ex.getMessage());
		}
	}

	public List<ServDocumentoLinea> getLineas() {
		return lineas;
	}

	public void setLineas(List<ServDocumentoLinea> lineas) {
		this.lineas = lineas;
	}
}
