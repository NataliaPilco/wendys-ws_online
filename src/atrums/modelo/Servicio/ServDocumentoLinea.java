package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class ServDocumentoLinea {
	static final Logger log = Logger.getLogger(ServDocumentoLinea.class);
	private String nroDocumento = null;
	private String tipoDoc = null;
	private String fechaFactura = null;
	private String fechaContable = null;
	private String cliente = null;
	private String destinatario = null;
	private String transportita = null;
	
	private String subTotal = null;
	private String totalFac = null;
	private String nroEstablecimiento = null;
	private String nroEmision = null;
	private String producto = null;
	private String cantidad = null;
	private String precio = null;
	private String totalLinea = null;
	private String descuento = null;
	private String impuesto = null;
	private String valorImpuesto = null;
	private String baseImponible = null;
	private String nombreComercial = null;
	private String razonSocial = null;
	
	private String metodoPago = null;
	
	private String sucursal = null;
	private String nroFactura = null;
	private String proveedor = null;
	private String rucProveedor = null;
	private String dirProveedor = null;
	private String fechaEmision = null;
	private String nroComprobanteVenta = null;
	private String totalRetencion = null;
	private String tipoDocumento = null;
	private String fechaConta = null;
	
	private String numDocRelacionado = null;
	private String fechaNumDocRelaciona = null;
	
	private String dirPartida = null;
	private String nombreComercialTransp = null;
	private String razonSocialTransp = null;
	private String fechaIniTransp = null;
	private String fechaFinTransp = null;
	private String placa = null;
	private String dirDestinatario = null;
	private String motivoTras = null;
	private String descripcion = null;
	
	private String mensajeError = null;
	
	private List<ServPago> pagos = new ArrayList<ServPago>();
	
	public ServDocumentoLinea(){
		super();
	}
	
	public List<ServPago> getPagos() {
		return pagos;
	}

	public void setPagos(List<ServPago> pagos) {
		this.pagos = pagos;
	}

	public ServDocumentoLinea(String nroDocumento, 
			String tipoDoc, 
			String fechaFactura, 
			String fechaContable, 
			String cliente, 
			String subTotal, 
			String totalFac, 
			String nroEstablecimiento, 
			String nroEmision, 
			String producto, 
			String cantidad, 
			String precio, 
			String totalLinea, 
			String descuento, 
			String impuesto, 
			String valorImpuesto, 
			String baseImponible, 
			String nombreComercial, 
			String razonSocial, 
			String numDocRelacionado, 
			String fechaNumDocRelaciona,
			String mensajeError) {
		this.nroDocumento = nroDocumento;
		this.tipoDoc = tipoDoc;
		this.fechaFactura = fechaFactura;
		this.fechaContable = fechaContable;
		this.cliente = cliente;
		this.subTotal = subTotal;
		this.totalFac = totalFac;
		this.nroEstablecimiento = nroEstablecimiento;
		this.nroEmision = nroEmision;
		this.producto = producto;
		this.cantidad = cantidad;
		this.precio = precio;
		this.totalLinea = totalLinea;
		this.impuesto = impuesto;
		this.valorImpuesto = valorImpuesto;
		this.baseImponible = baseImponible;
		this.nombreComercial = nombreComercial;
		this.razonSocial = razonSocial;
		this.numDocRelacionado = numDocRelacionado;
		this.fechaNumDocRelaciona = fechaNumDocRelaciona;
		this.mensajeError = mensajeError;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getMotivoTras() {
		return motivoTras;
	}

	public void setMotivoTras(String motivoTras) {
		this.motivoTras = motivoTras;
	}

	public String getDirDestinatario() {
		return dirDestinatario;
	}

	public void setDirDestinatario(String dirDestinatario) {
		this.dirDestinatario = dirDestinatario;
	}

	public String getFechaIniTransp() {
		return fechaIniTransp;
	}

	public void setFechaIniTransp(String fechaIniTransp) {
		this.fechaIniTransp = fechaIniTransp;
	}

	public String getFechaFinTransp() {
		return fechaFinTransp;
	}

	public void setFechaFinTransp(String fechaFinTransp) {
		this.fechaFinTransp = fechaFinTransp;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getNombreComercialTransp() {
		return nombreComercialTransp;
	}

	public void setNombreComercialTransp(String nombreComercialTransp) {
		this.nombreComercialTransp = nombreComercialTransp;
	}

	public String getRazonSocialTransp() {
		return razonSocialTransp;
	}

	public void setRazonSocialTransp(String razonSocialTransp) {
		this.razonSocialTransp = razonSocialTransp;
	}

	public String getTransportita() {
		return transportita;
	}

	public void setTransportita(String transportita) {
		this.transportita = transportita;
	}

	public String getDirPartida() {
		return dirPartida;
	}

	public void setDirPartida(String dirPartida) {
		this.dirPartida = dirPartida;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getMetodoPago() {
		return metodoPago;
	}

	public void setMetodoPago(String metodoPago) {
		this.metodoPago = metodoPago;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getRucProveedor() {
		return rucProveedor;
	}

	public void setRucProveedor(String rucProveedor) {
		this.rucProveedor = rucProveedor;
	}

	public String getDirProveedor() {
		return dirProveedor;
	}

	public void setDirProveedor(String dirProveedor) {
		this.dirProveedor = dirProveedor;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getNroComprobanteVenta() {
		return nroComprobanteVenta;
	}

	public void setNroComprobanteVenta(String nroComprobanteVenta) {
		this.nroComprobanteVenta = nroComprobanteVenta;
	}

	public String getTotalRetencion() {
		return totalRetencion;
	}

	public void setTotalRetencion(String totalRetencion) {
		this.totalRetencion = totalRetencion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getFechaConta() {
		return fechaConta;
	}

	public void setFechaConta(String fechaConta) {
		this.fechaConta = fechaConta;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getNumDocRelacionado() {
		return numDocRelacionado;
	}

	public void setNumDocRelacionado(String numDocRelacionado) {
		this.numDocRelacionado = numDocRelacionado;
	}

	public String getFechaNumDocRelaciona() {
		return fechaNumDocRelaciona;
	}

	public void setFechaNumDocRelaciona(String fechaNumDocRelaciona) {
		this.fechaNumDocRelaciona = fechaNumDocRelaciona;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getTipoDoc() {
		return tipoDoc;
	}

	public void setTipoDoc(String tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	public String getFechaFactura() {
		return fechaFactura;
	}

	public void setFechaFactura(String fechaFactura) {
		this.fechaFactura = fechaFactura;
	}

	public String getFechaContable() {
		return fechaContable;
	}

	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(String subTotal) {
		this.subTotal = subTotal;
	}

	public String getTotalFac() {
		return totalFac;
	}

	public void setTotalFac(String totalFac) {
		this.totalFac = totalFac;
	}

	public String getNroEstablecimiento() {
		return nroEstablecimiento;
	}

	public void setNroEstablecimiento(String nroEstablecimiento) {
		this.nroEstablecimiento = nroEstablecimiento;
	}

	public String getNroEmision() {
		return nroEmision;
	}

	public void setNroEmision(String nroEmision) {
		this.nroEmision = nroEmision;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getTotalLinea() {
		return totalLinea;
	}

	public void setTotalLinea(String totalLinea) {
		this.totalLinea = totalLinea;
	}

	public String getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(String impuesto) {
		this.impuesto = impuesto;
	}

	public String getValorImpuesto() {
		return valorImpuesto;
	}

	public void setValorImpuesto(String valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}

	public String getBaseImponible() {
		return baseImponible;
	}

	public void setBaseImponible(String baseImponible) {
		this.baseImponible = baseImponible;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
}
