package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public class ServDocumentosSucNumFNumDocProve {
	static final Logger log = Logger.getLogger(ServDocumentosSucNumFNumDocProve.class);
	List<ServDocumentoSucNumFNumDocProve> documentos;
	
	public ServDocumentosSucNumFNumDocProve(SOAPMessage soapMes){
		documentos = new ArrayList<ServDocumentoSucNumFNumDocProve>();
        try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO").getLength();
                if(tamanio != 0){
                    NodeList datosDoc = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO");
                    NodeList datosSuc = soapMes.getSOAPBody().getElementsByTagName("NO_ESTABLECIMIENTO");
                    //soapMes.getSOAPBody().getElementsByTagName("SUCURSAL");
                    NodeList datosNroFac = soapMes.getSOAPBody().getElementsByTagName("PTO_EMISION");
                    //soapMes.getSOAPBody().getElementsByTagName("NUM_FACTURA");
                    NodeList datosProvee = soapMes.getSOAPBody().getElementsByTagName("PROVEEDOR");
                
                    for(int i=0; i<tamanio; i++){
                    	ServDocumentoSucNumFNumDocProve auxDoc = new ServDocumentoSucNumFNumDocProve(
                    			datosSuc.item(i).getFirstChild().getNodeValue(), 
                    			datosNroFac.item(i).getFirstChild().getNodeValue(), 
                    			datosDoc.item(i).getFirstChild().getNodeValue(), 
                    			"RT", 
                    			datosProvee.item(i).getFirstChild().getNodeValue());
                        documentos.add(auxDoc);
                    }
                }
            }
        } catch(DOMException ex){
        	log.error(ex.getMessage());
        } catch(SOAPException ex){
        	log.error(ex.getMessage());
        }
	}

	public List<ServDocumentoSucNumFNumDocProve> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<ServDocumentoSucNumFNumDocProve> documentos) {
		this.documentos = documentos;
	}
}
