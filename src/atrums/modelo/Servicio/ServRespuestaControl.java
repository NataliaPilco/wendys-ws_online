package atrums.modelo.Servicio;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

public class ServRespuestaControl {
	static final Logger log = Logger.getLogger(ServRespuestaControl.class);
	private boolean respuesta = false;
	
	public ServRespuestaControl() {
		super();
	}
	
	public ServRespuestaControl(SOAPMessage soapMes) {
		try{
			if(soapMes != null){
				int tamanio = soapMes.getSOAPBody().getElementsByTagName("InsertarFE_Control_DocumentosResult").getLength();
				if(tamanio != 0){
					String auxRepsuesta = soapMes.getSOAPBody().getElementsByTagName("InsertarFE_Control_DocumentosResult").item(0).getFirstChild().getNodeValue();
					
					if(auxRepsuesta.equals("ok")){
						this.respuesta = true;
					}
				}
			}
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	public boolean isRespuesta() {
		return respuesta;
	}

	public void setRespuesta(boolean respuesta) {
		this.respuesta = respuesta;
	}
}
