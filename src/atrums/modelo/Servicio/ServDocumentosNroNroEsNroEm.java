package atrums.modelo.Servicio;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.NodeList;

public class ServDocumentosNroNroEsNroEm {
	static final Logger log = Logger.getLogger(ServDocumentosNroNroEsNroEm.class);
	List<ServDocumentoNroNroEsNroEm> documentos;
	
	public ServDocumentosNroNroEsNroEm(SOAPMessage soapMes, String fecha){
		documentos = new ArrayList<ServDocumentoNroNroEsNroEm>();
        try{
            if(soapMes != null){
                int tamanio = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO").getLength();
                if(tamanio != 0){
                    NodeList datosDoc = soapMes.getSOAPBody().getElementsByTagName("NUM_DOCUMENTO");
                    
                    NodeList datosNroEs = soapMes.getSOAPBody().getElementsByTagName("NO_ESTABLECIMIENTO");
                    
                    if (datosNroEs.getLength() == 0){
                    	datosNroEs = soapMes.getSOAPBody().getElementsByTagName("sucursal");
                    	if(datosNroEs.getLength() == 0){
                    		datosNroEs = soapMes.getSOAPBody().getElementsByTagName("PTO_ESTABLECIMIENTO");
                    	}
                    }
                    
                    NodeList datosNroEm = soapMes.getSOAPBody().getElementsByTagName("PTO_EMISION");
                    NodeList datosNroTi = soapMes.getSOAPBody().getElementsByTagName("TIPODOC");
                
                    if(datosNroTi.getLength() == 0){
                    	datosNroTi =  soapMes.getSOAPBody().getElementsByTagName("TIPO_DOC");
                    }
                    
                    for(int i=0; i<tamanio; i++){
                    	ServDocumentoNroNroEsNroEm auxDoc = new ServDocumentoNroNroEsNroEm(
                    			datosDoc.item(i).getFirstChild().getNodeValue(), 
                    			datosNroEs.item(i).getFirstChild().getNodeValue(), 
                    			datosNroEm.item(i).getFirstChild().getNodeValue(), 
                    			datosNroTi.item(i).getFirstChild().getNodeValue(), 
                    			fecha);
                        documentos.add(auxDoc);
                    }
                    
                    datosNroTi = null;
                    datosNroEm = null;
                    datosNroEs = null; 
                    datosDoc = null;
                }
            }
            
            soapMes = null;
        } catch(DOMException ex){
        	log.error(ex.getMessage());
        } catch(SOAPException ex){
        	log.error(ex.getMessage());
        }
	}

	public List<ServDocumentoNroNroEsNroEm> getDocumentos() {
		return documentos;
	}

	public void setDocumentos(List<ServDocumentoNroNroEsNroEm> documentos) {
		this.documentos = documentos;
	}
}
