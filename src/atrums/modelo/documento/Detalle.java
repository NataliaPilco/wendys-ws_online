package atrums.modelo.documento;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Detalle {
	private static Logger log = Logger.getLogger(Detalle.class);
	private String codigoPrincipal;
	private String codigoAuxiliar;
	private String codigoInterno;
	private String codigoAdicional;
	private String descripcion;
	private String cantidad;
	private String precioUnitario;
	private String descuento;
	private String precioTotalSinImpuesto;
	private List<Impuesto> impuestos;
	
	public Detalle(Element element){
		try {
			if(element.getElementsByTagName("codigoPrincipal").getLength() > 0){
				this.codigoPrincipal = element.getElementsByTagName("codigoPrincipal").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("codigoAuxiliar").getLength() > 0){
				this.codigoAuxiliar = element.getElementsByTagName("codigoAuxiliar").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("codigoInterno").getLength() > 0){
				this.codigoInterno = element.getElementsByTagName("codigoInterno").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("codigoAdicional").getLength() > 0){
				this.codigoAdicional = element.getElementsByTagName("codigoAdicional").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("descripcion").getLength() > 0){
				this.descripcion = element.getElementsByTagName("descripcion").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("cantidad").getLength() > 0){
				this.cantidad = element.getElementsByTagName("cantidad").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("precioUnitario").getLength() > 0){
				this.precioUnitario = element.getElementsByTagName("precioUnitario").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("descuento").getLength() > 0){
				this.descuento = element.getElementsByTagName("descuento").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("precioTotalSinImpuesto").getLength() > 0){
				this.precioTotalSinImpuesto = element.getElementsByTagName("precioTotalSinImpuesto").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("impuestos").getLength() > 0){
				List<Impuesto> auxImpuestos = new ArrayList<Impuesto>();
				NodeList auxNodes = element.getElementsByTagName("impuestos");
				for(int j = 0; j < auxNodes.getLength(); j++) {
					Element auxElement = (Element) auxNodes.item(j);
					if(auxElement.getElementsByTagName("impuesto").getLength() > 0){
						NodeList auxNodess = auxElement.getElementsByTagName("impuesto");
						for(int k = 0; k < auxNodess.getLength(); k++){
							Impuesto impuesto = new Impuesto((Element) auxNodess.item(k));
							auxImpuestos.add(impuesto);
						}
					}
				}
				this.impuestos = auxImpuestos;
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}
	
	public String getCodigoPrincipal() {
		return codigoPrincipal;
	}
	
	public void setCodigoPrincipal(String codigoPrincipal) {
		this.codigoPrincipal = codigoPrincipal;
	}
	
	public String getCodigoAuxiliar() {
		return codigoAuxiliar;
	}
	
	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getCodigoAdicional() {
		return codigoAdicional;
	}

	public void setCodigoAdicional(String codigoAdicional) {
		this.codigoAdicional = codigoAdicional;
	}

	public void setCodigoAuxiliar(String codigoAuxiliar) {
		this.codigoAuxiliar = codigoAuxiliar;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getCantidad() {
		return cantidad;
	}
	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
	public String getPrecioUnitario() {
		return precioUnitario;
	}
	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}
	public String getDescuento() {
		return descuento;
	}
	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}
	public String getPrecioTotalSinImpuesto() {
		return precioTotalSinImpuesto;
	}
	public void setPrecioTotalSinImpuesto(String precioTotalSinImpuesto) {
		this.precioTotalSinImpuesto = precioTotalSinImpuesto;
	}
	public List<Impuesto> getImpuestos() {
		return impuestos;
	}
	public void setImpuestos(List<Impuesto> impuestos) {
		this.impuestos = impuestos;
	}
}