package atrums.modelo.documento;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class InfoNotaCredito {
	private static Logger log = Logger.getLogger(InfoNotaCredito.class);
	private String fechaEmision;
	private String dirEstablecimiento;
	private String tipoIdentificacionComprador;
	private String razonSocialComprador;
	private String identificacionComprador;
	private String contribuyenteEspecial;
	private String obligadoContabilidad;
	private String codDocModificado;
	private String numDocModificado;
	private String fechaEmisionDocSustento;
	private String totalSinImpuestos;
	private String valorModificacion;
	private String moneda;
	private List<TotalImpuesto> totalConImpuestos;
	private String motivo;
	
	public InfoNotaCredito(String Documento){
		try {
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			InputSource source = new InputSource();
			source.setCharacterStream(new StringReader(Documento));
			Document doc = documentBuilder.parse(source);
			NodeList nodes = doc.getElementsByTagName("infoNotaCredito");
			
			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				
				if(element.getElementsByTagName("fechaEmision").getLength() > 0){
					this.fechaEmision = element.getElementsByTagName("fechaEmision").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("dirEstablecimiento").getLength() > 0){
					this.dirEstablecimiento = element.getElementsByTagName("dirEstablecimiento").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("tipoIdentificacionComprador").getLength() > 0){
					this.tipoIdentificacionComprador = element.getElementsByTagName("tipoIdentificacionComprador").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("razonSocialComprador").getLength() > 0){
					this.razonSocialComprador = element.getElementsByTagName("razonSocialComprador").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("identificacionComprador").getLength() > 0){
					this.identificacionComprador = element.getElementsByTagName("identificacionComprador").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("contribuyenteEspecial").getLength() > 0){
					this.contribuyenteEspecial = element.getElementsByTagName("contribuyenteEspecial").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("obligadoContabilidad").getLength() > 0){
					this.obligadoContabilidad = element.getElementsByTagName("obligadoContabilidad").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("codDocModificado").getLength() > 0){
					this.codDocModificado = element.getElementsByTagName("codDocModificado").item(0).getFirstChild().getNodeValue();
				}
				
				if(element.getElementsByTagName("numDocModificado").getLength() > 0){
					this.numDocModificado = element.getElementsByTagName("numDocModificado").item(0).getFirstChild().getNodeValue();
				}

				if(element.getElementsByTagName("fechaEmisionDocSustento").getLength() > 0){
					this.fechaEmisionDocSustento = element.getElementsByTagName("fechaEmisionDocSustento").item(0).getFirstChild().getNodeValue();
				}

				if(element.getElementsByTagName("totalSinImpuestos").getLength() > 0){
					this.totalSinImpuestos = element.getElementsByTagName("totalSinImpuestos").item(0).getFirstChild().getNodeValue();
				}

				if(element.getElementsByTagName("valorModificacion").getLength() > 0){
					this.valorModificacion = element.getElementsByTagName("valorModificacion").item(0).getFirstChild().getNodeValue();
				}

				if(element.getElementsByTagName("moneda").getLength() > 0){
					this.moneda = element.getElementsByTagName("moneda").item(0).getFirstChild().getNodeValue();
				}
				if(element.getElementsByTagName("totalConImpuestos").getLength() > 0){
					List<TotalImpuesto> auxTotalConImpuestos = new ArrayList<TotalImpuesto>();
					NodeList auxNodes = element.getElementsByTagName("totalConImpuestos");
					for(int j = 0; j < auxNodes.getLength(); j++) {
						Element auxElement = (Element) auxNodes.item(j);
						if(auxElement.getElementsByTagName("totalImpuesto").getLength() > 0){
							NodeList auxNodess = auxElement.getElementsByTagName("totalImpuesto");
							for(int k = 0; k < auxNodess.getLength(); k++){
								TotalImpuesto totalImpuesto = new TotalImpuesto((Element) auxNodess.item(k));
								auxTotalConImpuestos.add(totalImpuesto);
							}
						}
					}
					this.totalConImpuestos = auxTotalConImpuestos;
				}
				
				if(element.getElementsByTagName("motivo").getLength() > 0){
					this.motivo = element.getElementsByTagName("motivo").item(0).getFirstChild().getNodeValue();
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}
	
	public String getFechaEmision() {
		return fechaEmision;
	}
	
	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	
	public String getDirEstablecimiento() {
		return dirEstablecimiento;
	}
	
	public void setDirEstablecimiento(String dirEstablecimiento) {
		this.dirEstablecimiento = dirEstablecimiento;
	}
	
	public String getTipoIdentificacionComprador() {
		return tipoIdentificacionComprador;
	}
	
	public void setTipoIdentificacionComprador(String tipoIdentificacionComprador) {
		this.tipoIdentificacionComprador = tipoIdentificacionComprador;
	}
	
	public String getRazonSocialComprador() {
		return razonSocialComprador;
	}
	
	public void setRazonSocialComprador(String razonSocialComprador) {
		this.razonSocialComprador = razonSocialComprador;
	}
	
	public String getIdentificacionComprador() {
		return identificacionComprador;
	}
	
	public void setIdentificacionComprador(String identificacionComprador) {
		this.identificacionComprador = identificacionComprador;
	}
	
	public String getContribuyenteEspecial() {
		return contribuyenteEspecial;
	}
	
	public void setContribuyenteEspecial(String contribuyenteEspecial) {
		this.contribuyenteEspecial = contribuyenteEspecial;
	}
	
	public String getObligadoContabilidad() {
		return obligadoContabilidad;
	}
	
	public void setObligadoContabilidad(String obligadoContabilidad) {
		this.obligadoContabilidad = obligadoContabilidad;
	}
	
	public String getCodDocModificado() {
		return codDocModificado;
	}
	
	public void setCodDocModificado(String codDocModificado) {
		this.codDocModificado = codDocModificado;
	}
	
	public String getNumDocModificado() {
		return numDocModificado;
	}
	
	public void setNumDocModificado(String numDocModificado) {
		this.numDocModificado = numDocModificado;
	}
	
	public String getFechaEmisionDocSustento() {
		return fechaEmisionDocSustento;
	}
	
	public void setFechaEmisionDocSustento(String fechaEmisionDocSustento) {
		this.fechaEmisionDocSustento = fechaEmisionDocSustento;
	}
	
	public String getTotalSinImpuestos() {
		return totalSinImpuestos;
	}
	
	public void setTotalSinImpuestos(String totalSinImpuestos) {
		this.totalSinImpuestos = totalSinImpuestos;
	}
	
	public String getValorModificacion() {
		return valorModificacion;
	}
	
	public void setValorModificacion(String valorModificacion) {
		this.valorModificacion = valorModificacion;
	}
	
	public String getMoneda() {
		return moneda;
	}
	
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
	public List<TotalImpuesto> getTotalConImpuestos() {
		return totalConImpuestos;
	}
	
	public void setTotalConImpuestos(List<TotalImpuesto> totalConImpuestos) {
		this.totalConImpuestos = totalConImpuestos;
	}
	
	public String getMotivo() {
		return motivo;
	}
	
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}	
}