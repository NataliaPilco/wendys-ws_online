package atrums.modelo.documento;

import org.apache.log4j.Logger;
import org.w3c.dom.Element;

public class TotalImpuesto {
	private static Logger log = Logger.getLogger(TotalImpuesto.class);
	private String codigo;
	private String codigoPorcentaje;
	private String baseImponible;
	private String valor;
	
	public TotalImpuesto(Element element){
		try {
			if(element.getElementsByTagName("codigo").getLength() > 0){
				this.codigo = element.getElementsByTagName("codigo").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("codigoPorcentaje").getLength() > 0){
				this.codigoPorcentaje = element.getElementsByTagName("codigoPorcentaje").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("baseImponible").getLength() > 0){
				this.baseImponible = element.getElementsByTagName("baseImponible").item(0).getFirstChild().getNodeValue();
			}
			
			if(element.getElementsByTagName("valor").getLength() > 0){
				this.valor = element.getElementsByTagName("valor").item(0).getFirstChild().getNodeValue();
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getCodigoPorcentaje() {
		return codigoPorcentaje;
	}
	public void setCodigoPorcentaje(String codigoPorcentaje) {
		this.codigoPorcentaje = codigoPorcentaje;
	}
	public String getBaseImponible() {
		return baseImponible;
	}
	public void setBaseImponible(String baseImponible) {
		this.baseImponible = baseImponible;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
}