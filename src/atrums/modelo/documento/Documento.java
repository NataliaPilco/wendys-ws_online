package atrums.modelo.documento;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

public class Documento {
	private static Logger log = Logger.getLogger(Documento.class);
	private Document document;

	public Documento (String direccionxml){
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			this.document = documentBuilder.parse(direccionxml);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage() + " - Cause: " + e.getCause());
		}
	}
	
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
}
