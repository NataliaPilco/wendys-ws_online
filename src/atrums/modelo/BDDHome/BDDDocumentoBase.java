package atrums.modelo.BDDHome;

import java.util.UUID;

import org.apache.log4j.Logger;

public class BDDDocumentoBase {
	static final Logger log = Logger.getLogger(BDDDocumentoBase.class);
	private String documentosbaseid = null;
	private String nrodocumento = null;
	private String nroestablecimiento = null;
	private String nroemision = null;
	private String tipodocumento = null;
	private String fechadocumento = null;
	private String claveacceso = null;
	private String nroautorizacion = null;
	private String fechaautorizacion = null;
	private String usuario = null;
	private String password = null;
	private String estado = "N";
	private String mensaje = null;
	
	public BDDDocumentoBase() {
		super();
	}
	
	public BDDDocumentoBase(String nrodocumento, 
			String nroestablecimiento, 
			String nroemision, 
			String tipodocumento, 
			String fechadocumento){
		this.documentosbaseid = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		this.nrodocumento = nrodocumento;
		this.nroestablecimiento = nroestablecimiento;
		this.nroemision = nroemision;
		this.tipodocumento = tipodocumento;
		this.fechadocumento = fechadocumento;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getDocumentosbaseid() {
		return documentosbaseid;
	}

	public void setDocumentosbaseid(String documentosbaseid) {
		this.documentosbaseid = documentosbaseid;
	}

	public String getNrodocumento() {
		return nrodocumento;
	}

	public void setNrodocumento(String nrodocumento) {
		this.nrodocumento = nrodocumento;
	}

	public String getNroestablecimiento() {
		return nroestablecimiento;
	}

	public void setNroestablecimiento(String nroestablecimiento) {
		this.nroestablecimiento = nroestablecimiento;
	}

	public String getNroemision() {
		return nroemision;
	}

	public void setNroemision(String nroemision) {
		this.nroemision = nroemision;
	}

	public String getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getFechadocumento() {
		return fechadocumento;
	}

	public void setFechadocumento(String fechadocumento) {
		this.fechadocumento = fechadocumento;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getNroautorizacion() {
		return nroautorizacion;
	}

	public void setNroautorizacion(String nroautorizacion) {
		this.nroautorizacion = nroautorizacion;
	}

	public String getFechaautorizacion() {
		return fechaautorizacion;
	}

	public void setFechaautorizacion(String fechaautorizacion) {
		this.fechaautorizacion = fechaautorizacion;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
}
