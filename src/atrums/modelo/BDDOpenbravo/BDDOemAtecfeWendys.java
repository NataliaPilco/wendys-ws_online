package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOemAtecfeWendys {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String nroDocumento = null;
	private String nroEmision = null;
	private String nroEstablecimiento = null;
	private String tipoDocumento = null;
	private String estado = null;
	private String observacion = null;
	
	public BDDOemAtecfeWendys() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getNroEmision() {
		return nroEmision;
	}

	public void setNroEmision(String nroEmision) {
		this.nroEmision = nroEmision;
	}

	public String getNroEstablecimiento() {
		return nroEstablecimiento;
	}

	public void setNroEstablecimiento(String nroEstablecimiento) {
		this.nroEstablecimiento = nroEstablecimiento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
}
