package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcInvoiceLine {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String linea = null;
	private String cantidad = null;
	private String precioUnitario = null;
	private String descuento = null;
	private String precioTotal = null;
	private String valor = null;
	
	public BDDOcInvoiceLine() {
		super();
	}

	public String getDescuento() {
		return descuento;
	}

	public void setDescuento(String descuento) {
		this.descuento = descuento;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getPrecioUnitario() {
		return precioUnitario;
	}

	public void setPrecioUnitario(String precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public String getPrecioTotal() {
		return precioTotal;
	}

	public void setPrecioTotal(String precioTotal) {
		this.precioTotal = precioTotal;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
