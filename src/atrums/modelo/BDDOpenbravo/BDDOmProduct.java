package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOmProduct {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String codigo = null;
	private String name = null;
	
	public BDDOmProduct() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
