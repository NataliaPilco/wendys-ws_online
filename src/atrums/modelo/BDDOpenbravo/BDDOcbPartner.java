package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcbPartner {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String razonSocial = null;
	private String nombreComercial = null;
	private String tipo = null;
	private String identificacion = null;
	private String email = null;
	
	public BDDOcbPartner(){
		super();
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
}
