package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcbGroup {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String nombre = "CLIENTES COMERCIALES";
	
	public BDDOcbGroup() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
