package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOadRoleOrgaccess {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	
	public BDDOadRoleOrgaccess() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
