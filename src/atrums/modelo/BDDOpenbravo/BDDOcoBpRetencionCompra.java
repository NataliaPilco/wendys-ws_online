package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcoBpRetencionCompra {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String description = null;
	
	public BDDOcoBpRetencionCompra() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
