package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOmInoutLine {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String linea = null;
	private String cantidad = null;
	private String descripcion = null;
	
	public BDDOmInoutLine() {
		super();
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
