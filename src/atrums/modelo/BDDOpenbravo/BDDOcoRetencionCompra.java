package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcoRetencionCompra {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String nroDocumento = null;
	private String fechaEmision = null;
	private String fechaContable = null;
	private String numeroAutorizacion = null;
	private String fechaAutorizacion = null;
	private String nroEstablecimiento = null;
	private String nroEmision = null;
	private String estado = null;
	private String claveAcceso = null;
	private String tipoDocumento = null;
	private String descripcion = null;
	private String nrofactura = null;
	private String retencion = null;
	private String identificacion = null;
	
	public BDDOcoRetencionCompra() {
		super();
	}
	
	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getRetencion() {
		return retencion;
	}

	public void setRetencion(String retencion) {
		this.retencion = retencion;
	}

	public String getNrofactura() {
		return nrofactura;
	}

	public void setNrofactura(String nrofactura) {
		this.nrofactura = nrofactura;
	}

	public String getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(String fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public String getFechaContable() {
		return fechaContable;
	}

	public void setFechaContable(String fechaContable) {
		this.fechaContable = fechaContable;
	}

	public String getNumeroAutorizacion() {
		return numeroAutorizacion;
	}

	public void setNumeroAutorizacion(String numeroAutorizacion) {
		this.numeroAutorizacion = numeroAutorizacion;
	}

	public String getFechaAutorizacion() {
		return fechaAutorizacion;
	}

	public void setFechaAutorizacion(String fechaAutorizacion) {
		this.fechaAutorizacion = fechaAutorizacion;
	}

	public String getNroEstablecimiento() {
		return nroEstablecimiento;
	}

	public void setNroEstablecimiento(String nroEstablecimiento) {
		this.nroEstablecimiento = nroEstablecimiento;
	}

	public String getNroEmision() {
		return nroEmision;
	}

	public void setNroEmision(String nroEmision) {
		this.nroEmision = nroEmision;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getClaveAcceso() {
		return claveAcceso;
	}

	public void setClaveAcceso(String claveAcceso) {
		this.claveAcceso = claveAcceso;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
