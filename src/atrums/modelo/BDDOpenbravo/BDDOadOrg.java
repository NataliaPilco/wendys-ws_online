package atrums.modelo.BDDOpenbravo;

public class BDDOadOrg {
	private String id = null;
	private String name = null;
	
	public BDDOadOrg(){
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
