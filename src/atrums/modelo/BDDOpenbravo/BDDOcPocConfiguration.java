package atrums.modelo.BDDOpenbravo;

public class BDDOcPocConfiguration {
	private String smtpserver = null;
	private String smtpserveracount = null;
	private String smtppassword = null;
	private boolean issmtpautorization = false;
	private String smtpsegurity = null;
	private String smtpport = null;
	
	public BDDOcPocConfiguration() {
		super();
	}

	public String getSmtpserver() {
		return smtpserver;
	}

	public void setSmtpserver(String smtpserver) {
		this.smtpserver = smtpserver;
	}

	public String getSmtpserveracount() {
		return smtpserveracount;
	}

	public void setSmtpserveracount(String smtpserveracount) {
		this.smtpserveracount = smtpserveracount;
	}

	public String getSmtppassword() {
		return smtppassword;
	}

	public void setSmtppassword(String smtppassword) {
		this.smtppassword = smtppassword;
	}

	public boolean isIssmtpautorization() {
		return issmtpautorization;
	}

	public void setIssmtpautorization(boolean issmtpautorization) {
		this.issmtpautorization = issmtpautorization;
	}

	public String getSmtpsegurity() {
		return smtpsegurity;
	}

	public void setSmtpsegurity(String smtpsegurity) {
		this.smtpsegurity = smtpsegurity;
	}

	public String getSmtpport() {
		return smtpport;
	}

	public void setSmtpport(String smtpport) {
		this.smtpport = smtpport;
	}
}
