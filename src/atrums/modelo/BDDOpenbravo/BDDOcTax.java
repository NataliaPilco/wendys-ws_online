package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcTax {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String rate = null;
	
	public BDDOcTax() {
		super();
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
