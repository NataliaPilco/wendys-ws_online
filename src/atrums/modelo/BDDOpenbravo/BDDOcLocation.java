package atrums.modelo.BDDOpenbravo;

import java.util.UUID;

public class BDDOcLocation {
	private String id = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	private String direccion = null;
	
	public BDDOcLocation() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
