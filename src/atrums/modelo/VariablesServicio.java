package atrums.modelo;

public class VariablesServicio {

	private boolean estadoFacturas = true;
	private boolean estadoNotas = true;
	private boolean estadoRetenciones = true;
	private boolean estadoGuiasRemision = true;
	private boolean estadoFacturasClave = true;
	
	private boolean semaforoUno = true;
	private boolean semaforoDos_A = true;
	private boolean semaforoDos_M = true;
	
	/**Variables para clases de monitoreo de claves de acceso**/
	private boolean extDocumentoFacClaveF = true;
	private boolean extDocumentoFacClaveN = true;
	
	/**Variables para clases de monitoreo de facturas**/
	private boolean extDocumentoFacMig = true;
	private boolean extDocumentoFacSRI = true;
	
	private int repeticionFacSRI = 0;
	private boolean continuarFacSRI = true;
	private String documentFacSRI = "";
	
	private int repeticionFacMig = 0;
	private boolean continuarFacMig = true;
	private String documentFacMig = "";

	/**Variables para clases de monitoreo de notas de credito**/
	private boolean extDocumentoNotMig = true;
	private boolean extDocumentoNotSRI = true;
	
	private int repeticionNotSRI = 0;
	private boolean continuarNotSRI = true;
	private String documentNotSRI = "";
	
	private int repeticionNotMig = 0;
	private boolean continuarNotMig = true;
	private String documentNotMig = "";
	
	/**Variables para clases de monitoreo de retenciones**/
	private boolean extDocumentoRetMig = true;
	private boolean extDocumentoRetSRI = true;
	
	private int repeticionRetSRI = 0;
	private boolean continuarRetSRI = true;
	private String documentRetSRI = "";
	
	private int repeticionRetMig = 0;
	private boolean continuarRetMig = true;
	private String documentRetMig = "";
	
	/**Variables para clases de monitoreo de guias**/
	private boolean extDocumentoGiaMig = true;
	private boolean extDocumentoGiaSRI = true;
	
	private int repeticionGiaSRI = 0;
	private boolean continuarGiaSRI = true;
	private String documentGiaSRI = "";
	
	private int repeticionGiaMig = 0;
	private boolean continuarGiaMig = true;
	private String documentGiaMig = "";
	
	public VariablesServicio() {
		// TODO Auto-generated constructor stub
		super();
	}

	public boolean isSemaforoDos_A() {
		return semaforoDos_A;
	}

	public void setSemaforoDos_A(boolean semaforoDos_A) {
		this.semaforoDos_A = semaforoDos_A;
	}

	public boolean isSemaforoDos_M() {
		return semaforoDos_M;
	}

	public void setSemaforoDos_M(boolean semaforoDos_M) {
		this.semaforoDos_M = semaforoDos_M;
	}

	public boolean isExtDocumentoGiaMig() {
		return extDocumentoGiaMig;
	}

	public void setExtDocumentoGiaMig(boolean extDocumentoGiaMig) {
		this.extDocumentoGiaMig = extDocumentoGiaMig;
	}

	public boolean isExtDocumentoGiaSRI() {
		return extDocumentoGiaSRI;
	}

	public void setExtDocumentoGiaSRI(boolean extDocumentoGiaSRI) {
		this.extDocumentoGiaSRI = extDocumentoGiaSRI;
	}

	public int getRepeticionGiaSRI() {
		return repeticionGiaSRI;
	}

	public void setRepeticionGiaSRI(int repeticionGiaSRI) {
		this.repeticionGiaSRI = repeticionGiaSRI;
	}

	public boolean isContinuarGiaSRI() {
		return continuarGiaSRI;
	}

	public void setContinuarGiaSRI(boolean continuarGiaSRI) {
		this.continuarGiaSRI = continuarGiaSRI;
	}

	public String getDocumentGiaSRI() {
		return documentGiaSRI;
	}

	public void setDocumentGiaSRI(String documentGiaSRI) {
		this.documentGiaSRI = documentGiaSRI;
	}

	public int getRepeticionGiaMig() {
		return repeticionGiaMig;
	}

	public void setRepeticionGiaMig(int repeticionGiaMig) {
		this.repeticionGiaMig = repeticionGiaMig;
	}

	public boolean isContinuarGiaMig() {
		return continuarGiaMig;
	}

	public void setContinuarGiaMig(boolean continuarGiaMig) {
		this.continuarGiaMig = continuarGiaMig;
	}

	public String getDocumentGiaMig() {
		return documentGiaMig;
	}

	public void setDocumentGiaMig(String documentGiaMig) {
		this.documentGiaMig = documentGiaMig;
	}

	public boolean isEstadoNotas() {
		return estadoNotas;
	}

	public void setEstadoNotas(boolean estadoNotas) {
		this.estadoNotas = estadoNotas;
	}

	public boolean isExtDocumentoNotMig() {
		return extDocumentoNotMig;
	}

	public void setExtDocumentoNotMig(boolean extDocumentoNotMig) {
		this.extDocumentoNotMig = extDocumentoNotMig;
	}

	public boolean isExtDocumentoNotSRI() {
		return extDocumentoNotSRI;
	}

	public void setExtDocumentoNotSRI(boolean extDocumentoNotSRI) {
		this.extDocumentoNotSRI = extDocumentoNotSRI;
	}

	public int getRepeticionNotSRI() {
		return repeticionNotSRI;
	}

	public void setRepeticionNotSRI(int repeticionNotSRI) {
		this.repeticionNotSRI = repeticionNotSRI;
	}

	public boolean isContinuarNotSRI() {
		return continuarNotSRI;
	}

	public void setContinuarNotSRI(boolean continuarNotSRI) {
		this.continuarNotSRI = continuarNotSRI;
	}

	public String getDocumentNotSRI() {
		return documentNotSRI;
	}

	public void setDocumentNotSRI(String documentNotSRI) {
		this.documentNotSRI = documentNotSRI;
	}

	public int getRepeticionNotMig() {
		return repeticionNotMig;
	}

	public void setRepeticionNotMig(int repeticionNotMig) {
		this.repeticionNotMig = repeticionNotMig;
	}

	public boolean isContinuarNotMig() {
		return continuarNotMig;
	}

	public void setContinuarNotMig(boolean continuarNotMig) {
		this.continuarNotMig = continuarNotMig;
	}

	public String getDocumentNotMig() {
		return documentNotMig;
	}

	public void setDocumentNotMig(String documentNotMig) {
		this.documentNotMig = documentNotMig;
	}

	public int getRepeticionFacMig() {
		return repeticionFacMig;
	}

	public void setRepeticionFacMig(int repeticionFacMig) {
		this.repeticionFacMig = repeticionFacMig;
	}

	public boolean isContinuarFacMig() {
		return continuarFacMig;
	}

	public void setContinuarFacMig(boolean continuarFacMig) {
		this.continuarFacMig = continuarFacMig;
	}

	public String getDocumentFacMig() {
		return documentFacMig;
	}

	public void setDocumentFacMig(String documentFacMig) {
		this.documentFacMig = documentFacMig;
	}

	public String getDocumentFacSRI() {
		return documentFacSRI;
	}

	public void setDocumentFacSRI(String documentFacSRI) {
		this.documentFacSRI = documentFacSRI;
	}

	public boolean isEstadoFacturas() {
		return estadoFacturas;
	}

	public void setEstadoFacturas(boolean estadoFacturas) {
		this.estadoFacturas = estadoFacturas;
	}

	public int getRepeticionFacSRI() {
		return repeticionFacSRI;
	}

	public void setRepeticionFacSRI(int repeticionFacSRI) {
		this.repeticionFacSRI = repeticionFacSRI;
	}

	public boolean isContinuarFacSRI() {
		return continuarFacSRI;
	}

	public void setContinuarFacSRI(boolean continuarFacSRI) {
		this.continuarFacSRI = continuarFacSRI;
	}

	public boolean isExtDocumentoFacMig() {
		return extDocumentoFacMig;
	}

	public void setExtDocumentoFacMig(boolean extDocumentoFacMig) {
		this.extDocumentoFacMig = extDocumentoFacMig;
	}

	public boolean isExtDocumentoFacSRI() {
		return extDocumentoFacSRI;
	}

	public void setExtDocumentoFacSRI(boolean extDocumentoFacSRI) {
		this.extDocumentoFacSRI = extDocumentoFacSRI;
	}

	public boolean isExtDocumentoFacClaveF() {
		return extDocumentoFacClaveF;
	}

	public void setExtDocumentoFacClaveF(boolean extDocumentoFacClaveF) {
		this.extDocumentoFacClaveF = extDocumentoFacClaveF;
	}

	public boolean isExtDocumentoFacClaveN() {
		return extDocumentoFacClaveN;
	}

	public void setExtDocumentoFacClaveN(boolean extDocumentoFacClaveN) {
		this.extDocumentoFacClaveN = extDocumentoFacClaveN;
	}

	public boolean isEstadoFacturasClave() {
		return estadoFacturasClave;
	}

	public void setEstadoFacturasClave(boolean estadoFacturasClave) {
		this.estadoFacturasClave = estadoFacturasClave;
	}

	public boolean isEstadoGuiasRemision() {
		return estadoGuiasRemision;
	}

	public void setEstadoGuiasRemision(boolean estadoGuiasRemision) {
		this.estadoGuiasRemision = estadoGuiasRemision;
	}

	public boolean isSemaforoUno() {
		return semaforoUno;
	}

	public void setSemaforoUno(boolean semaforoUno) {
		this.semaforoUno = semaforoUno;
	}

	public boolean isExtDocumentoRetMig() {
		return extDocumentoRetMig;
	}

	public void setExtDocumentoRetMig(boolean extDocumentoRetMig) {
		this.extDocumentoRetMig = extDocumentoRetMig;
	}

	public boolean isExtDocumentoRetSRI() {
		return extDocumentoRetSRI;
	}

	public void setExtDocumentoRetSRI(boolean extDocumentoRetSRI) {
		this.extDocumentoRetSRI = extDocumentoRetSRI;
	}

	public int getRepeticionRetSRI() {
		return repeticionRetSRI;
	}

	public void setRepeticionRetSRI(int repeticionRetSRI) {
		this.repeticionRetSRI = repeticionRetSRI;
	}

	public boolean isContinuarRetSRI() {
		return continuarRetSRI;
	}

	public void setContinuarRetSRI(boolean continuarRetSRI) {
		this.continuarRetSRI = continuarRetSRI;
	}

	public String getDocumentRetSRI() {
		return documentRetSRI;
	}

	public void setDocumentRetSRI(String documentRetSRI) {
		this.documentRetSRI = documentRetSRI;
	}

	public int getRepeticionRetMig() {
		return repeticionRetMig;
	}

	public void setRepeticionRetMig(int repeticionRetMig) {
		this.repeticionRetMig = repeticionRetMig;
	}

	public boolean isContinuarRetMig() {
		return continuarRetMig;
	}

	public void setContinuarRetMig(boolean continuarRetMig) {
		this.continuarRetMig = continuarRetMig;
	}

	public boolean isEstadoRetenciones() {
		return estadoRetenciones;
	}

	public void setEstadoRetenciones(boolean estadoRetenciones) {
		this.estadoRetenciones = estadoRetenciones;
	}

	public String getDocumentRetMig() {
		return documentRetMig;
	}

	public void setDocumentRetMig(String documentRetMig) {
		this.documentRetMig = documentRetMig;
	}
}
