package atrums.modelo.SRI;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;
import org.w3c.dom.DOMException;

public class SRIDocumentoRecibido {
	static final Logger log = Logger.getLogger(SRIDocumentoRecibido.class);
	private String estado = null;
	private String estadoespecifico = null;
	private String mensaje = null;
	private String informacion = null;
	
	public SRIDocumentoRecibido(){
		super();
	}
	
	public SRIDocumentoRecibido(SOAPMessage soapMessage) {
		try {
			if(soapMessage != null){
				if(soapMessage.getSOAPBody().getElementsByTagName("estado").getLength() > 0){
					this.estado = soapMessage.getSOAPBody().getElementsByTagName("estado").item(0).getFirstChild().getNodeValue();
					this.estadoespecifico = this.estado.substring(0, 3);
				}else{
					this.estado = "NORECIBIDO";
					this.estadoespecifico = this.estado.substring(0, 3);
					this.mensaje = "ERROR DE CONEXION / ";
					this.informacion = "No esta disponible el servicio web del SRI";
				}
				
				if(soapMessage.getSOAPBody().getElementsByTagName("mensaje").getLength() > 0){
					this.mensaje = soapMessage.getSOAPBody().getElementsByTagName("mensaje").item(1).getFirstChild().getNodeValue() + " / ";
				}
				
				if(soapMessage.getSOAPBody().getElementsByTagName("informacionAdicional").getLength() > 0){
					this.informacion = soapMessage.getSOAPBody().getElementsByTagName("informacionAdicional").item(0).getFirstChild().getNodeValue();
				}
			}else{
				this.estado = "NORECIBIDO";
				this.estadoespecifico = this.estado.substring(0, 3);
				this.mensaje = "ERROR DE CONEXION / ";
				this.informacion = "No esta disponible el servicio web del SRI";
			}
		} catch (DOMException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		} catch (SOAPException ex) {
			// TODO Auto-generated catch block
			log.error(ex.getMessage());
		}
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEstadoespecifico() {
		return estadoespecifico;
	}

	public void setEstadoespecifico(String estadoespecifico) {
		this.estadoespecifico = estadoespecifico;
	}
}
